package com.pgedunesia.edunesia.student.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.NilaiUjian;
import com.pgedunesia.edunesia.parent.adapter.AdapterRekapNilai;
import com.pgedunesia.edunesia.student.ui.home.HomeStudentViewModel;
import com.pgedunesia.edunesia.student.ui.list_mapel.ListMapelActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExamValueFragment extends Fragment {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.recycler_ujian)
    RecyclerView recycler_ujian;
    @BindView(R.id.progress_ujian)
    ProgressBar progressBar;
    @BindView(R.id.view_quiz_zero)
    LinearLayout view_empty;
    @BindView(R.id.view_content)
    LinearLayout view_content;
    @BindView(R.id.button_mapel)
    Button btn_mapel;
    @BindView(R.id.text_empty)
    TextView text_empty;

    HomeStudentViewModel mViewModel;
    AdapterRekapNilai adapterRekapNilai;

    View view;

    int siswa_id = 0;

    public ExamValueFragment() {
        // Required empty public constructor
    }

    public static ExamValueFragment newInstance(int siswa_id) {
        ExamValueFragment fragment = new ExamValueFragment();
        Bundle args = new Bundle();
        args.putInt("SISWA_ID", siswa_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            siswa_id = getArguments().getInt("SISWA_ID");
        }
        adapterRekapNilai = new AdapterRekapNilai(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_nilai_ujian, container, false);
        ButterKnife.bind(this, view);

        progressBar.setVisibility(View.VISIBLE);
        mViewModel = ViewModelProviders.of(this).get(HomeStudentViewModel.class);
        if (siswa_id == 0) {
            mViewModel.loadNilaiUjian(PrefManager.getInstance(getContext()).getSiswa().getId());
            Log.d("SISWA_ID", "Pref " + String.valueOf(siswa_id));
        } else {
            mViewModel.loadNilaiUjian(siswa_id);
            Log.d("SISWA_ID", String.valueOf(siswa_id));
        }
        mViewModel.getNilaiUjian().observe(this, new Observer<List<NilaiUjian>>() {
            @Override
            public void onChanged(List<NilaiUjian> nilaiUjians) {
                if (nilaiUjians != null) {
                    if (nilaiUjians.size() > 0) {
                        view_content.setVisibility(View.VISIBLE);
                        view_empty.setVisibility(View.GONE);
                        adapterRekapNilai.updateData(nilaiUjians);
                    } else {
                        view_content.setVisibility(View.GONE);
                        view_empty.setVisibility(View.VISIBLE);
                        if (siswa_id != 0) {
                            btn_mapel.setVisibility(View.GONE);
                            text_empty.setText("Ooops! Nilai ujiannya masih kosong");
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                if (siswa_id == 0) {
                    mViewModel.loadNilaiUjian(PrefManager.getInstance(getContext()).getSiswa().getId());
                } else {
                    mViewModel.loadNilaiUjian(siswa_id);
                }
            }
        });

        recycler_ujian.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_ujian.setAdapter(adapterRekapNilai);
        return view;
    }

    @OnClick(R.id.button_mapel)
    void onClickButtonMapel() {
        startActivity(new Intent(getContext(), ListMapelActivity.class));
    }

}
