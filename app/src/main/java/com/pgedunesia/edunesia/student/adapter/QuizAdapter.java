package com.pgedunesia.edunesia.student.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Quiz;
import com.pgedunesia.edunesia.student.ui.quiz_popup.QuizPopupActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.QuizViewHolder> {

    private Context mContext;
    private List<Quiz> quizzes=  new ArrayList<>();

    public QuizAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public class QuizViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.text_judul)
        TextView text_judul;
        @BindView(R.id.text_mapel)
        TextView text_mapel;
        @BindView(R.id.image_quiz)
        ImageView image_quiz;

        public QuizViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }

    public void updateData(List<Quiz> quizzes) {
        this.quizzes = quizzes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public QuizViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QuizViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quiz, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QuizViewHolder holder, int position) {
        Quiz quiz = quizzes.get(position);

        holder.text_judul.setText(quiz.getJudul());
        holder.text_mapel.setText(quiz.getMapel() + " " + quiz.getKelas());
        CommonUtils.load_image(mContext, holder.image_quiz, quiz.getGambar());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, QuizPopupActivity.class);
                intent.putExtra("quiz", (Serializable) quiz);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return quizzes.size();
    }
}
