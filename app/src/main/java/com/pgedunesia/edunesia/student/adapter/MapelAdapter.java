package com.pgedunesia.edunesia.student.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Mapel;
import com.pgedunesia.edunesia.student.ui.ujian.UjianActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapelAdapter extends RecyclerView.Adapter<MapelAdapter.ViewHolder> implements Filterable {

    private Context context;
    private List<Mapel> mapelList = new ArrayList<>();
    private List<Mapel> mFilteredList = new ArrayList<>();

    public MapelAdapter(Context context) {
        this.context = context;
    }

    public MapelAdapter(Context context, List<Mapel> mapelList) {
        this.context = context;
        this.mapelList = mapelList;
        this.mFilteredList = mapelList;
    }

    public void updateData(List<Mapel> mapelList) {
        this.mapelList = mapelList;
        this.mFilteredList = mapelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mapel, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mapel mapel = mFilteredList.get(position);

        if (mapel.getNama().length() > 15){
            holder.text_nama.setText(mapel.getAlias());
        }else{
            holder.text_nama.setText(mapel.getNama());
        }

        holder.text_desc.setText(mapel.getDeskripsi());

        CommonUtils.load_image(context, holder.image_mapel, mapel.getGambar());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, UjianActivity.class);
            intent.putExtra("ID", mapel.getId());
            intent.putExtra("NAME", mapel.getNama());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mapelList;
                } else {

                    List<Mapel> filteredList = new ArrayList<>();

                    for (Mapel androidVersion : mapelList) {

                        if (androidVersion.getNama().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (List<Mapel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_nama_mapel)
        TextView text_nama;
        @BindView(R.id.text_desc_mapel)
        TextView text_desc;
        @BindView(R.id.image_mapel)
        ImageView image_mapel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
