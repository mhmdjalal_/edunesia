package com.pgedunesia.edunesia.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.NilaiUjian;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder>{

    private Context context;
    private List<NilaiUjian> ujians = new ArrayList<>();

    public ScoreAdapter(Context context) {
        this.context = context;
    }

    public ScoreAdapter(Context context, List<NilaiUjian> ujians) {
        this.context = context;
        this.ujians = ujians;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_score, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NilaiUjian ujian = ujians.get(position);

        holder.name.setText(ujian.getMapel());
        holder.time.setText(ujian.getTanggal());
        holder.type.setText(ujian.getUjian());
        holder.score.setText(String.valueOf(ujian.getNilai()));
        holder.level.setText(" ( " + ujian.getLevel() + " ) ");

    }

    @Override
    public int getItemCount() {
        return ujians.size();
    }

    public void updateData(List<NilaiUjian> ujians) {
        this.ujians = ujians;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_score)
        TextView score;
        @BindView(R.id.text_mapel_name)
        TextView name;
        @BindView(R.id.text_type_ujian)
        TextView type;
        @BindView(R.id.text_time)
        TextView time;
        @BindView(R.id.text_level)
        TextView level;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
