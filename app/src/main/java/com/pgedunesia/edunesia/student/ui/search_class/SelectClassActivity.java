package com.pgedunesia.edunesia.student.ui.search_class;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Class;
import com.pgedunesia.edunesia.student.adapter.ClassAdapter;
import com.pgedunesia.edunesia.student.viewmodel.ProfileViewModel;

import java.util.List;

public class SelectClassActivity extends AppCompatActivity {

    @BindView(R.id.recycler_class)
    RecyclerView recyclerView;
    @BindView(R.id.edittext_search)
    EditText etSearch;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    ClassAdapter adapter;
    ProfileViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_class);
        ButterKnife.bind(this);

        adapter = new ClassAdapter(this);

        viewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        viewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading != null) {
                    Log.d("isloading", "isloading2? " + isLoading);
                    if (isLoading) {
                        progressBar.setVisibility(View.VISIBLE);
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        });
        viewModel.getKelases().observe(this, new Observer<List<Class>>() {
            @Override
            public void onChanged(List<Class> classes) {
                if (classes != null) {
                    adapter.updateDate(classes);
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.getFilter().filter(editable.toString());
            }
        });
    }

    @OnClick(R.id.image_back)
    void onClickBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
