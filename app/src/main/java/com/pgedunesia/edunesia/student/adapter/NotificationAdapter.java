package com.pgedunesia.edunesia.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.StudentNotification;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>{

    private Context context;
    private List<StudentNotification> studentNotificationList;

    public NotificationAdapter(Context context, List<StudentNotification> studentNotificationList) {
        this.context = context;
        this.studentNotificationList = studentNotificationList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student_notif, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        StudentNotification studentNotification = studentNotificationList.get(position);

        holder.title.setText(studentNotification.getTitle());
        holder.date.setText(studentNotification.getDate());
        holder.desc.setText(studentNotification.getMapel());

    }

    @Override
    public int getItemCount() {
        return studentNotificationList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_title_notif)
        TextView title;
        @BindView(R.id.text_desc_notif)
        TextView desc;
        @BindView(R.id.text_date_notif)
        TextView date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
