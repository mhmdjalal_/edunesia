package com.pgedunesia.edunesia.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefUjian;
import com.pgedunesia.edunesia.data.model.Soal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizNumberAdapter extends RecyclerView.Adapter<QuizNumberAdapter.ViewHolder>{

    private Context context;
    private List<Soal> soalList;
    final OnItemClickListener listener;
    PrefUjian prefUjian;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public QuizNumberAdapter(Context context, List<Soal> soalList, OnItemClickListener listener) {
        this.context = context;
        this.soalList = soalList;
        this.listener = listener;
        prefUjian = PrefUjian.getInstance(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_number, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int number = position+1;
        holder.number.setText(String.valueOf(number));

        String get_jawaban = prefUjian.getSoalJawaban(String.valueOf(number));
        if (!get_jawaban.equals("")){
            holder.relativeLayout.setBackground(context.getResources().getDrawable(R.drawable.bg_circle));
            holder.number.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }else{
            holder.number.setTextColor(context.getResources().getColor(android.R.color.white));
        }

        holder.itemView.setOnClickListener(view -> {
            listener.onItemClick(number);
            //holder.number.setForeground(context.getResources().getDrawable(R.drawable.bg_circle));
            holder.number.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        });
    }

    @Override
    public int getItemCount() {
        return soalList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_number)
        TextView number;
        @BindView(R.id.relative)
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

    public void setColorLastNumber(){

        LayoutInflater layoutInflater = (LayoutInflater)context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view1 = layoutInflater.inflate(R.layout.item_number, null);
        TextView textnumber = view1.findViewById(R.id.text_number);

        String get_jawaban_last = prefUjian.getSoalJawaban(String.valueOf(soalList.size()));
        if (!get_jawaban_last.equals("")){
            view1.findViewById(R.id.relative).setBackground(context.getResources().getDrawable(R.drawable.bg_circle));
           textnumber.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }else{
            textnumber.setTextColor(context.getResources().getColor(android.R.color.white));
        }
    }

}
