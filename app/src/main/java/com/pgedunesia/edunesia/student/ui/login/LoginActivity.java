package com.pgedunesia.edunesia.student.ui.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.response.LoginSiswaResponse;
import com.pgedunesia.edunesia.student.ui.home.HomeActivity;
import com.pgedunesia.edunesia.student.ui.register.RegisterActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.edittext_email)
    EditText editText_email;
    @BindView(R.id.edittext_password)
    EditText editText_password;
    @BindView(R.id.row_top)
    FrameLayout row_top;
    @BindView(R.id.bottom_section)
    LinearLayout bottom_section;

    LoginStudentViewModel mViewModel;
    PrefManager prefManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        responsiveScreen();

        prefManager = new PrefManager(this);
        progressDialog = new ProgressDialog(this);

        Log.d("selisih_date", String.valueOf(CommonUtils.checkDate()) + " hari");

        mViewModel = ViewModelProviders.of(this).get(LoginStudentViewModel.class);
        mViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading != null) {
                    Log.d("isloading", "isloading2? " + isLoading);
                    if (isLoading) {
                        progressDialog.setMessage("Loading...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }
        });
        mViewModel.getLoginResponse().observe(this, new Observer<LoginSiswaResponse>() {
            @Override
            public void onChanged(LoginSiswaResponse loginSiswaResponse) {
                if (loginSiswaResponse != null) {
                    if (loginSiswaResponse.getStatus()) {
                        prefManager.setSiswa(loginSiswaResponse.getStudent());
                        prefManager.setIsLogin(true);
                        prefManager.setIsSiswa(true);
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                    } else {
                        Snackbar.make(editText_password, loginSiswaResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        loadAnimation();

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        );
    }

    void loadAnimation() {
        Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        bottom_section.startAnimation(slideUp);
        Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        row_top.startAnimation(slideDown);
    }

    @OnClick(R.id.view_daftar)
    void onClick() {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.image_back)
    void onClickBack() {
        super.onBackPressed();
    }

    @Optional
    @OnClick(R.id.button_login)
    void onClickNext() {
        if (editText_email.getText().toString().trim().isEmpty()) {
            editText_email.requestFocus();
            editText_email.setError("Email kosong");
        } else if (editText_password.getText().toString().trim().isEmpty()) {
            editText_password.requestFocus();
            editText_password.setError("Password kosng");
        } else {
            mViewModel.loginSiswa(editText_email.getText().toString().trim(), editText_password.getText().toString().trim());
        }
    }

    void responsiveScreen() {
        double screenSize = CommonUtils.getScreenDimension(this);
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
            row_top.setLayoutParams(params);
        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 380);
            row_top.setLayoutParams(params);
        }
    }
}
