package com.pgedunesia.edunesia.student.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Branch;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.ViewHolder> implements Filterable {

    private Activity activity;
    private List<Branch> branchs = new ArrayList<>();
    private List<Branch> mFilteredList = new ArrayList<>();

    public BranchAdapter(Activity activity) {
        this.activity = activity;
    }

    public void updateDate(List<Branch> branchs) {
        this.branchs = branchs;
        this.mFilteredList = branchs;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selection, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Branch branch = mFilteredList.get(position);
        holder.text_nama.setText(branch.getNama());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("branch", branch);
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.text_nama)
        TextView text_nama;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = branchs;
                } else {

                    List<Branch> filteredList = new ArrayList<>();

                    for (Branch branch : branchs) {

                        if (branch.getNama().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(branch);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (List<Branch>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
