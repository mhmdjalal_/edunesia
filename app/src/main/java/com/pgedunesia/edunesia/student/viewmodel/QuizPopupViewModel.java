package com.pgedunesia.edunesia.student.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.response.BaseResponse;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizPopupViewModel extends AndroidViewModel {
    private MutableLiveData<BaseResponse> mResponse = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsLoading = new MutableLiveData<>();

    ApiInterface apiInterface;
    PrefManager prefManager;

    public QuizPopupViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        prefManager = PrefManager.getInstance(getApplication());
    }

    public LiveData<BaseResponse> getSendResponse() {
        return mResponse;
    }

    public LiveData<Boolean> isLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.postValue(isLoading);
    }

    public void sendJawabanQuiz(int quiz_id, String photoPath) {
        File imageFile = new File(photoPath);
        RequestBody propertyImage = RequestBody.create(MediaType.parse("image/*"), imageFile);
        MultipartBody.Part file = MultipartBody.Part.createFormData("gambar", imageFile.getName(), propertyImage);

        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("quiz_id", CommonUtils.createPartFromString(String.valueOf(quiz_id)));
        body.put("siswa_id", CommonUtils.createPartFromString(String.valueOf(prefManager.getSiswa().getId())));
        body.put("cabang_id", CommonUtils.createPartFromString(String.valueOf(prefManager.getSiswa().getCabangId())));

        apiInterface.storeJawabanQuizPopup(body, file)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        mResponse.postValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {

                    }
                });
    }
}
