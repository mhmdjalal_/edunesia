package com.pgedunesia.edunesia.student.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.response.NilaiQuizResponse;
import com.pgedunesia.edunesia.data.response.ReportNilaiResponse;
import com.pgedunesia.edunesia.student.adapter.NilaiPagerAdapter;
import com.pgedunesia.edunesia.student.ui.home.HomeStudentViewModel;
import com.pgedunesia.edunesia.student.ui.notification.StudentNotifActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportValueFragment extends Fragment {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    View view;

    HomeStudentViewModel mViewModel;
    NilaiPagerAdapter pagerAdapter;

    NilaiQuizResponse mQuizzes;
    ReportNilaiResponse mNilaiUjians;

    public static ReportValueFragment newInstance(){
        ReportValueFragment fragment = new ReportValueFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_score, container, false);
        ButterKnife.bind(this, view);

        pagerAdapter = new NilaiPagerAdapter(getChildFragmentManager());
        pagerAdapter.addFragment(ExamValueFragment.newInstance(0), "Nilai Ujian");
        pagerAdapter.addFragment(QuizValueFragment.newInstance(0), "Nilai Quiz");
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.image_notification)
    void onClickNotification() {
        startActivity(new Intent(getContext(), StudentNotifActivity.class));
    }
}
