package com.pgedunesia.edunesia.student.ui.quiz;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.config.AppConfig;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.config.PrefUjian;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.student.adapter.PembahasanAdapter;
import com.pgedunesia.edunesia.data.model.Soal;
import com.pgedunesia.edunesia.data.response.SoalResponse;
import com.pgedunesia.edunesia.student.ui.home.HomeActivity;
import com.pgedunesia.edunesia.student.viewmodel.MasterViewModel;
import com.pgedunesia.edunesia.student.viewmodel.ProfileViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScoreBoardActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_pembahasan)
    RecyclerView recyclerView;
    @BindView(R.id.text_name)
    TextView text_name;
    @BindView(R.id.text_ujian_name)
    TextView ujian_name;
    @BindView(R.id.text_ujian_mapel)
    TextView ujian_mapel;
    @BindView(R.id.text_ujian_waktu)
    TextView ujian_waktu;
    @BindView(R.id.text_jumlah_soal_benar)
    TextView jumlah_benar;
    @BindView(R.id.text_jumlah_soal_salah)
    TextView jumlah_salah;
    @BindView(R.id.text_ujian_level)
    TextView ujian_level;
    @BindView(R.id.text_score_value)
    TextView score_value;
    @BindView(R.id.linear)
    LinearLayout linearLayout;
    private PembahasanAdapter adapter;
    private List<Soal> questionsList;
    private ApiInterface apiInterface;
    private String ujianId;
    private int jumlah_soal;
    private ProfileViewModel profileViewModel;
    private MasterViewModel masterViewModel;
    private PrefManager prefManager;
    private PrefUjian prefUjian;
    double sum_benar, sum_salah, score;
    String get_pref_answer, get_pref_answer_salah;
    int score_value2;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_board);

        ButterKnife.bind(this);
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        masterViewModel = ViewModelProviders.of(this).get(MasterViewModel.class);
        prefManager = PrefManager.getInstance(this);
        prefUjian = PrefUjian.getInstance(this);
        setupToolbar(toolbar, getResources().getDrawable(R.drawable.ic_arrow_back_black));

        adapter = new PembahasanAdapter(this);

        Intent data = getIntent();
        ujianId = data.getStringExtra("UJIAN_ID");

        show_loading();
        request_siswa();
        request_ujian();
        load();
        show_dialog();
        clear_answer();
    }


    private void show_loading(){
        showLoading("Tunggu Sebentar");
        new Handler().postDelayed(() -> {
            dismissLoading();
            linearLayout.setVisibility(View.VISIBLE);
        }, AppConfig.DELAY_LOADING);
    }

    private void show_dialog(){
        new Handler().postDelayed(() -> {
            try{
                exit_pembahasan("Sudah paham ? Keluar dari pembahasan");
            }catch (WindowManager.BadTokenException e){
                Log.e("ERROR", Objects.requireNonNull(e.getMessage()));
            }
        }, AppConfig.DELAY_ALERT);
    }

    @Override
    public boolean onSupportNavigateUp() {
       onBackPressed();
       return true;
    }

    @SuppressLint("SetTextI18n")
    private void request_siswa(){
        profileViewModel.requestSiswa(prefManager.getSiswa().getId());
        profileViewModel.getSiswa().observe(this, siswa -> text_name.setText("Hai, " + siswa.getNama()));
    }

    @SuppressLint("SetTextI18n")
    private void request_ujian(){
        masterViewModel.requestUjianByID(Integer.parseInt(ujianId));
        masterViewModel.getmUjianById().observe(this, ujian -> {
            ujian_name.setText(ujian.getNama());
            ujian_mapel.setText(ujian.getMapel());
            ujian_waktu.setText(ujian.getWaktu() + " Menit");
            ujian_level.setText( ujian.getLevel().substring(0, 1).toUpperCase() + ujian.getLevel().substring(1));
        });
    }

    @SuppressLint("SetTextI18n")
    private void load(){

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        questionsList  = new ArrayList<>();
        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        apiInterface.getSoal(Integer.parseInt(ujianId)).enqueue(new Callback<SoalResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @SuppressLint({"SetTextI18n", "Range"})
            @Override
            public void onResponse(Call<SoalResponse> call, Response<SoalResponse> response) {
                if (response.isSuccessful()){
                    questionsList = response.body().getData();
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(ScoreBoardActivity.this, DividerItemDecoration.VERTICAL);
                    recyclerView.addItemDecoration(dividerItemDecoration);
                    adapter.updateAdapter(questionsList);
                    recyclerView.setAdapter(adapter);

                    jumlah_soal = questionsList.size();
                    int jawaban_benar2 = 0;
                    int jawaban_salah2 = 0;

                    for (int i = 0; i < questionsList.size(); i++) {
                        Log.d("jawaban", prefUjian.getSoalJawaban(String.valueOf(i+1)) + " - " + questionsList.get(i).getJawaban_benar());
                        if (prefUjian.getSoalJawaban(String.valueOf(i+1)).equals(questionsList.get(i).getJawaban_benar())) {
                            jawaban_benar2++;
                        } else {
                            jawaban_salah2++;
                        }
                    }

                    get_pref_answer = String.valueOf(jawaban_benar2);
                    get_pref_answer_salah = String.valueOf(jawaban_salah2);

                    Log.e("JUMLAH _ BENAR2", get_pref_answer);
                    Log.e("JUMLAH _ SALAH2", get_pref_answer_salah);
                    Log.e("STATUS : ", "EMPTY INCORRECT");
                    jumlah_benar.setText(get_pref_answer  + " dari " + jumlah_soal + " Soal");
                    jumlah_salah.setText(get_pref_answer_salah  + " dari " + jumlah_soal + " Soal");

                    sum_benar = Double.parseDouble(String.valueOf(get_pref_answer));
                    sum_salah = Double.parseDouble(String.valueOf(get_pref_answer_salah));
                    score  = sum_benar / jumlah_soal * 100;
                    score_value2 = (int) score;
                    score_value.setText(String.valueOf(score_value2));

                    store_nilai_siswa();
                }
            }

            @Override
            public void onFailure(Call<SoalResponse> call, Throwable t) {
                Log.e("ERROR : ", Objects.requireNonNull(t.getMessage()));
            }
        });

    }

    private void store_nilai_siswa(){
        int siswa_id = prefManager.getSiswa().getId();
        int ujian_id = Integer.parseInt(ujianId);
        int score_value = (int) score;
        Log.e("SISWA ID", String.valueOf(siswa_id));
        Log.e("UJIAN ID", String.valueOf(ujian_id));
        Log.e("SCORE VALUE", String.valueOf(score_value));
        masterViewModel.storeNilaiSiswa(siswa_id, ujian_id, score_value);
    }

    @Override
    public void onBackPressed() {
        exit_pembahasan("Yakin keluar dari pembahasan ?");
    }

    private void exit_pembahasan(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(ScoreBoardActivity.this);

        View v = getLayoutInflater().inflate(R.layout.dialog_exit_pembahasan, null);
        TextView text_yes = v.findViewById(R.id.text_yes);
        TextView text_no = v.findViewById(R.id.text_no);
        TextView text_message = v.findViewById(R.id.text_message);
        text_message.setText(message);
        builder.setView(v);

        AlertDialog alert = builder.create();
        alert.show();
        text_yes.setOnClickListener(view -> {
            alert.dismiss();
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            clear_answer();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        });
        text_no.setOnClickListener(view -> alert.cancel());
    }

    private void clear_answer(){
        for (int i = 1; i<jumlah_soal+1; i++){
            prefUjian.setUserJawabanNull(i);
            prefUjian.setUserJawabanBenarNull("BENAR");
            prefUjian.setUserJawabanSalahNull("SALAH");
            prefUjian.setJumlahSoalNull();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        prefUjian.setUserJawabanBenarNull("BENAR");
        prefUjian.setUserJawabanSalahNull("SALAH");
        prefUjian.setJumlahSoalNull();
    }
}
