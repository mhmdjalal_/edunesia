package com.pgedunesia.edunesia.student.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Quiz;
import com.pgedunesia.edunesia.parent.adapter.AdapterNilaiQuiz;
import com.pgedunesia.edunesia.student.ui.home.HomeStudentViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizValueFragment extends Fragment {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.recycler_quiz)
    RecyclerView recycler_quiz;
    @BindView(R.id.progress_quiz)
    ProgressBar progressBar;
    @BindView(R.id.view_content)
    LinearLayout view_content;
    @BindView(R.id.view_quiz_zero)
    LinearLayout view_empty;
    @BindView(R.id.text_empty)
    TextView text_empty;

    HomeStudentViewModel mViewModel;
    AdapterNilaiQuiz adapterNilaiQuiz;

    View view;

    int siswa_id = 0;

    public QuizValueFragment() {
        // Required empty public constructor
    }

    public static QuizValueFragment newInstance(int siswa_id) {
        QuizValueFragment fragment = new QuizValueFragment();
        Bundle args = new Bundle();
        args.putInt("SISWA_ID", siswa_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            siswa_id = getArguments().getInt("SISWA_ID", 0);
        }
        adapterNilaiQuiz = new AdapterNilaiQuiz(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_nilai_quiz, container, false);
        ButterKnife.bind(this, view);

        progressBar.setVisibility(View.VISIBLE);
        mViewModel = ViewModelProviders.of(this).get(HomeStudentViewModel.class);
        if (siswa_id == 0) {
            mViewModel.loadNilaiQuiz(PrefManager.getInstance(getContext()).getSiswa().getId());
        } else {
            mViewModel.loadNilaiQuiz(siswa_id);
        }
        mViewModel.getNilaiQuiz().observe(this, new Observer<List<Quiz>>() {
            @Override
            public void onChanged(List<Quiz> quizzes) {
                if (quizzes != null) {
                    if (quizzes.size() > 0) {
                        view_content.setVisibility(View.VISIBLE);
                        view_empty.setVisibility(View.GONE);
                        adapterNilaiQuiz.updateData(quizzes);
                    } else {
                        view_content.setVisibility(View.GONE);
                        view_empty.setVisibility(View.VISIBLE);
                        if (siswa_id != 0) {
                            text_empty.setText("Ooops! Nilai quiznya masih kosong");
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                if (siswa_id == 0) {
                    mViewModel.loadNilaiQuiz(PrefManager.getInstance(getContext()).getSiswa().getId());
                } else {
                    mViewModel.loadNilaiQuiz(siswa_id);
                }
            }
        });

        recycler_quiz.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_quiz.setAdapter(adapterNilaiQuiz);

        return view;
    }

}
