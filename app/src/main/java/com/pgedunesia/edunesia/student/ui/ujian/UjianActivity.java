package com.pgedunesia.edunesia.student.ui.ujian;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.AppConfig;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.student.adapter.UjianAdapter;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.data.model.Ujian;
import com.pgedunesia.edunesia.student.viewmodel.MasterViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UjianActivity extends BaseActivity{

    @BindView(R.id.recycler_ujian)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.swipe_ujian)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.text_title)
    TextView text_title;
    @BindView(R.id.view_ujian_no_data)
    LinearLayout linearLayout;
    @BindView(R.id.progress_ujian)
    ProgressBar progressBar;

    private UjianAdapter adapter;
    private List<Ujian> ujianArrayList;

    private String mapelname;
    private int idUjian, idmapel, id_siswa;

    ApiInterface apiInterface;
    MasterViewModel mViewModel;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ujian);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        progressBar.setVisibility(View.VISIBLE);
        mViewModel = ViewModelProviders.of(this).get(MasterViewModel.class);
        prefManager = PrefManager.getInstance(this);

        init();
        load();

    }

    private void init(){
        swipeRefreshLayout.setOnRefreshListener(() -> {
            load();
            swipeRefreshLayout.setRefreshing(false);
        });

        Intent data = getIntent();
        if(data != null){
            idmapel = Integer.parseInt(data.getStringExtra("ID"));
            mapelname = data.getStringExtra("NAME");
        }
    }

    private void load(){
        swipeRefreshLayout.setRefreshing(true);
        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        id_siswa = prefManager.getSiswa().getId();
        mViewModel.requestUjian(id_siswa, idmapel);
        Log.e("ID", String.valueOf(prefManager.getSiswa().getId()));
        recyclerView = findViewById(R.id.recycler_ujian);
        recyclerView.setLayoutManager(new LinearLayoutManager(UjianActivity.this));
        mViewModel.getmUjian().observe(this, ujians -> {
            progressBar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            ujianArrayList = ujians;
            adapter = new UjianAdapter(getApplicationContext(), ujians, ujian -> {
                check_aktivasi(ujian.getId(), ujian.getWaktu());
            });
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (adapter.getItemCount() < 1){
                linearLayout.setVisibility(View.VISIBLE);
            }else{
                linearLayout.setVisibility(View.GONE);
            }
        });
    }

//    private void filter(String text){
//        ujianArrayList = new ArrayList<>();
//        List<Ujian> ujians = new ArrayList<>();
//        for (Ujian ujian : ujianArrayList){
//            String ujianss = ujian.getNama();
//            if (ujianss != null && ujians.contains(text)){
//                ujians.add(ujian);
//            }
//        }
//        adapter.updateList(ujians);
//
//    }

    @OnClick(R.id.image_back)
    void onClickImageBack() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_search, menu);
//
//        MenuItem search = menu.findItem(R.id.search_laporan);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void check_aktivasi(String idujian, String time){
        showLoading("Sedang mengecek akun anda apakah anda telah diaktivasi");
        new Handler().postDelayed(() -> {
            mViewModel.checkAktivasi(id_siswa, BeforeUjianActivity.class, idujian, time);
            dismissLoading();
        }, AppConfig.DELAY_LOADING);
    }
}
