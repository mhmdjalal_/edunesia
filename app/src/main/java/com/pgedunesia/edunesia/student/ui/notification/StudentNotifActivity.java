package com.pgedunesia.edunesia.student.ui.notification;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.student.adapter.NotificationAdapter;
import com.pgedunesia.edunesia.data.model.StudentNotification;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentNotifActivity extends BaseActivity {

    @BindView(R.id.swipe_notif)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_notif)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private NotificationAdapter notificationAdapter;
    private List<StudentNotification> studentNotifications = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_notif);

        ButterKnife.bind(this);
        setupToolbar(toolbar, getResources().getDrawable(R.drawable.ic_arrow_back_black));

        init();
    }

    @Override
    public void setUpActionBar(String title) {
        super.setUpActionBar(title);
    }

    private void init(){
        swipeRefreshLayout.setOnRefreshListener(() -> {
            load();
            swipeRefreshLayout.setRefreshing(false);
        });
        load();
    }

    private void load(){
        for (int i = 0; i < 10; i++) {
            studentNotifications.add(new StudentNotification("Judul "+(i+1), "Mapel "+(i+1), "20-Aug-2019"));
        }

        notificationAdapter = new NotificationAdapter(this, studentNotifications);
        initAdapter();
    }

    void initAdapter() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(notificationAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
