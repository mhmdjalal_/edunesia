package com.pgedunesia.edunesia.student.ui.quiz;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.config.PrefUjian;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.model.Soal;
import com.pgedunesia.edunesia.data.response.SoalResponse;
import com.pgedunesia.edunesia.student.adapter.QuizNumberAdapter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizActivity extends BaseActivity {

    @BindView(R.id.btn_a)
    Button answer_a;
    @BindView(R.id.btn_b)
    Button answer_b;
    @BindView(R.id.btn_c)
    Button answer_c;
    @BindView(R.id.btn_d)
    Button answer_d;
    @BindView(R.id.text_questions)
    TextView textquetions;
    @BindView(R.id.timeText)
    TextView timetext;
    @BindView(R.id.image_previous)
    ImageButton image_previous;
    @BindView(R.id.image_next)
    ImageButton image_next;
    @BindView(R.id.button_finish)
    Button button_finish;
    @BindView(R.id.text_questions_number)
    TextView questions_number;
    @BindView(R.id.progress_quiz)
    ProgressBar progressBar;
    @BindView(R.id.recycler_number)
    RecyclerView recyclerView;
    @BindView(R.id.linear)
    LinearLayout linearLayout;
    @BindView(R.id.relative)
    RelativeLayout relativeLayout;

    public List<Soal> soalList = new ArrayList<>();
    private Soal soal;
    ApiInterface apiInterface;
    int qid = 0, jumlah_isi, jumlah_belum;
    CountDownTimer countDownTimer;
    private static final String FORMAT = "%02d:%02d:%02d";
    String ujianId, ujianTime;
    private PrefUjian prefManager;
    int i, soalSize;

    private ArrayList<String> jawabanModels = new ArrayList<>();
    private QuizNumberAdapter quizNumberAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        ButterKnife.bind(this);
        prefManager = PrefUjian.getInstance(this);
        progressBar.setVisibility(View.VISIBLE);
        init();

        Intent data = getIntent();
        if(data != null){
            ujianId = data.getStringExtra("UJIAN_ID");
            ujianTime = data.getStringExtra("TIME");
        }

        updateQueSoalAndAnswer();
        new Handler().postDelayed(this::start_timer, 1200);

    }

    private void init(){
        answer_a.setOnClickListener(view -> {
            String a = soal.getJawaban_a();
            a = "a";
            int sum = soalList.size();

            if (a.equals(soal.getJawaban_benar())){
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, a);
                    } else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, a);
                    }

                } else{
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, a);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, a);
                    }
                    set_color_a();
                }

            }else{
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, a);
                    }else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, a);
                    }

                }else{
                    Log.e("JUMLAH SOAL", String.valueOf(sum));
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, a);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, a);
                    }
                    set_color_a();
                }
            }

        });

        answer_b.setOnClickListener(view -> {
            String b = soal.getJawaban_b();
            b = "b";
            int sum = soalList.size();

            String jawaban_benar = soal.getJawaban_benar();

            if (b.equals(soal.getJawaban_benar())){
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, b);
                    }else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, b);
                    }

                }else{
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, b);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, b);
                    }
                    set_color_b();
                }
            }else{
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, b);
                    }else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, b);
                    }

                }else{
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, b);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, b);
                    }
                    set_color_b();
                }

            }
        });

        answer_c.setOnClickListener(view -> {
            String c = soal.getJawaban_c();
            c = "c";
            int sum = soalList.size();

            String jawaban_benar = soal.getJawaban_benar();

            if (c.equals(soal.getJawaban_benar())){
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, c);
                    }else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, c);
                    }

                }else{
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, c);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, c);
                    }
                    set_color_c();
                }
            }else{
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, c);
                    }else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, c);
                    }

                }else{
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, c);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, c);
                    }
                    set_color_c();
                }

            }
        });

        answer_d.setOnClickListener(view -> {
            String d = soal.getJawaban_d();
            d = "d";
            int sum = soalList.size();

            if (d.equals(soal.getJawaban_benar())){
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, d);
                    }else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, d);
                    }

                }else{
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, d);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, d);
                    }
                    set_color_d();
                }
            }else{
                if (qid  < soalList.size() - 1){
                    next_questions();
                    if (prefManager.getSoalJawaban(String.valueOf(qid)) == null){
                        prefManager.setUserJawaban(qid, d);
                    }else{
                        prefManager.setUserJawabanNull(qid);
                        prefManager.setUserJawaban(qid, d);
                    }


                }else{
                    if (prefManager.getSoalJawaban(String.valueOf(sum)) == null){
                        prefManager.setUserJawaban(sum, d);
                    }else{
                        prefManager.setUserJawabanNull(sum);
                        prefManager.setUserJawaban(sum, d);
                    }
                    set_color_d();
                }

            }
        });

        image_previous.setOnClickListener(view -> previous_questions());

        image_next.setOnClickListener(view -> next_questions2());

        button_finish.setOnClickListener(view -> check_finish2());

    }

    private void check_finish2(){
        try{
            int soalsize = Integer.parseInt(prefManager.getJumlahSoal());
            int jumlahIsi = 0;

            // hitung jumlah soal yang sudah diisi
            for(int i = 1; i <= soalsize; i++){
                String get_jawaban = prefManager.getSoalJawaban(String.valueOf(i));
                Log.e("GET_JAWABAN", i + get_jawaban + " " + get_jawaban.isEmpty());
                if (!get_jawaban.isEmpty()) {
                    jumlahIsi++;
                }
            }

            // check jumlah soal yg sudah diisi
            if (jumlahIsi == soalsize) {
                finish_quiz();
            } else {
                View view = findViewById(android.R.id.content);
                Snackbar.make(view, "Ooops, Jawaban anda belum semuanya diiisi...", Snackbar.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Log.e("ERROR", Objects.requireNonNull(e.getMessage()));
        }

    }

    private void start_timer(){
        int ujiantime = Integer.parseInt(ujianTime);
        long milis = ujiantime * 60 * 1000;
        new CountDownTimer(milis, 1000) { // adjust the milli seconds here

            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            public void onTick(long millisUntilFinished) {
                timetext.setText(""+String.format(FORMAT,
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                timetext.setText("done!");
            }
        }.start();
    }

    private void finish_quiz(){
        Intent intent = new Intent(QuizActivity.this, ScoreBoardActivity.class);
        intent.putExtra("UJIAN_ID", ujianId);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void updateQueSoalAndAnswer(){
        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        apiInterface.getSoal(Integer.parseInt(ujianId)).enqueue(new Callback<SoalResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @SuppressLint({"SetTextI18n", "Range"})
            @Override
            public void onResponse(Call<SoalResponse> call, Response<SoalResponse> response) {
                if (response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.VISIBLE);
                    soalList = response.body().getData();
                    soal = soalList.get(qid);
                    load_number();
                    if (qid == 0){
                        image_previous.setImageAlpha(75);
                        image_previous.setEnabled(false);
                        image_previous.setClickable(false);
                    }else{
                        image_previous.setImageAlpha(255);
                        image_previous.setEnabled(true);
                        image_previous.setClickable(true);
                    }

                    questions_number.setText(qid + 1 + ". ");
                    Spanned spanned = Html.fromHtml(soal.getPertanyaan(), new ImageGetter(), null);
                    Spanned jawaban_a = Html.fromHtml(soal.getJawaban_a(), new ImageGetter(), null);
                    Spanned jawaban_b = Html.fromHtml(soal.getJawaban_b(), new ImageGetter(), null);
                    Spanned jawaban_c = Html.fromHtml(soal.getJawaban_c(), new ImageGetter(), null);
                    Spanned jawaban_d = Html.fromHtml(soal.getJawaban_d(), new ImageGetter(), null);
                    textquetions.setText(spanned);
                    answer_a.setText(jawaban_a);
                    answer_b.setText(jawaban_b);
                    answer_c.setText(jawaban_c);
                    answer_d.setText(jawaban_d);

                    soalSize = soalList.size();
                    prefManager.setJumlahSoal(soalSize);
                    for(int b = 0; b < soalSize; b++){
                        if(qid == b){
                            String get_answer = prefManager.getSoalJawaban(String.valueOf(b+1));
                            if (get_answer != null) {
                                Log.e("POSISI ANDA DI ", qid+1 + " : " + get_answer);
                                switch (get_answer) {
                                    case "a":
                                        answer_a.setBackground(getDrawable(R.drawable.bg_button_outline_2));
                                        answer_a.setTextColor(getColor(android.R.color.white));
                                        answer_b.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_b.setTextColor(getColor(android.R.color.black));
                                        answer_c.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_c.setTextColor(getColor(android.R.color.black));
                                        answer_d.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_d.setTextColor(getColor(android.R.color.black));
                                        break;
                                    case "b":
                                        answer_b.setBackground(getDrawable(R.drawable.bg_button_outline_2));
                                        answer_b.setTextColor(getColor(android.R.color.white));
                                        answer_a.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_a.setTextColor(getColor(android.R.color.black));
                                        answer_c.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_c.setTextColor(getColor(android.R.color.black));
                                        answer_d.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_d.setTextColor(getColor(android.R.color.black));
                                        break;
                                    case "c":
                                        answer_c.setBackground(getDrawable(R.drawable.bg_button_outline_2));
                                        answer_c.setTextColor(getColor(android.R.color.white));
                                        answer_a.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_a.setTextColor(getColor(android.R.color.black));
                                        answer_b.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_b.setTextColor(getColor(android.R.color.black));
                                        answer_d.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_d.setTextColor(getColor(android.R.color.black));
                                        break;
                                    case "d":
                                        answer_d.setBackground(getDrawable(R.drawable.bg_button_outline_2));
                                        answer_d.setTextColor(getColor(android.R.color.white));
                                        answer_a.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_a.setTextColor(getColor(android.R.color.black));
                                        answer_b.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_b.setTextColor(getColor(android.R.color.black));
                                        answer_c.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_c.setTextColor(getColor(android.R.color.black));
                                        break;
                                    default:
                                        answer_a.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_a.setTextColor(getColor(android.R.color.black));
                                        answer_b.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_b.setTextColor(getColor(android.R.color.black));
                                        answer_c.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_c.setTextColor(getColor(android.R.color.black));
                                        answer_d.setBackground(getDrawable(R.drawable.bg_button_outline));
                                        answer_d.setTextColor(getColor(android.R.color.black));
                                        break;
                                }
                            }

                        }
                    }

                    if (qid == soalList.size()-1){
                        image_next.setVisibility(View.GONE);
                        button_finish.setVisibility(View.VISIBLE);
                    }else{
                        image_next.setVisibility(View.VISIBLE);
                        button_finish.setVisibility(View.GONE);
                    }

                    for(i = 0; i<soalSize+1; i++){
                        if (prefManager.getSoalJawaban(String.valueOf(i)) != null){
                            Log.e("Jawaban soal nomor " + i, prefManager.getSoalJawaban(String.valueOf(i)));
                        }else{
                            Log.e("Jawaban soal nomor " + i, "NULL");
                        }

                    }

                    String jumlah_jawaban_benar = prefManager.getJawabanBenar(qid+ 1 + "a");
                    Log.e("JUMLAH JAWABAN BENAR", jumlah_jawaban_benar);


                }
            }

            @Override
            public void onFailure(Call<SoalResponse> call, Throwable t) {
                Log.e("ERROR : ", Objects.requireNonNull(t.getMessage()));
            }
        });

    }

    private void load_number(){
        quizNumberAdapter = new QuizNumberAdapter(getApplicationContext(), soalList, new QuizNumberAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                qid = position-1;
                updateQueSoalAndAnswer();
            }
        });
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(quizNumberAdapter);
    }

    private void next_questions(){
        qid++;
        soal = soalList.get(qid);
        updateQueSoalAndAnswer();
    }

    private void next_questions2(){
        if (qid  < soalList.size() - 1){
            qid++;
            soal = soalList.get(qid);
            updateQueSoalAndAnswer();
        }else{
            finish_quiz();
        }
    }

    private void previous_questions(){
        if (qid == 0){
            onBackPressed();
        }else{
            image_previous.setEnabled(true);
            image_previous.setClickable(true);
            qid--;
            soal = soalList.get(qid);
            updateQueSoalAndAnswer();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (countDownTimer != null){
            countDownTimer.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (countDownTimer != null){
            countDownTimer.cancel();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (countDownTimer != null){
            countDownTimer.cancel();
        }
    }

    private class ImageGetter implements Html.ImageGetter {

        public Drawable getDrawable(String source) {
            LevelListDrawable d = new LevelListDrawable();
            Drawable empty = getResources().getDrawable(R.drawable.ic_launcher_foreground);
            d.addLevel(0, 0, empty);
            d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

            new LoadImage().execute(source, d);

            return d;
        }
    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d("TAG", "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                Log.e("URL", source);
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("URL", e.getMessage());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.e("URL", e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("URL", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d("TAG", "onPostExecute drawable " + mDrawable);
            Log.d("TAG", "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t = textquetions.getText();
                CharSequence a = answer_a.getText();
                CharSequence b = answer_b.getText();
                CharSequence c = answer_c.getText();
                CharSequence dd = answer_d.getText();
                textquetions.setText(t);
                answer_a.setText(a);
                answer_b.setText(b);
                answer_c.setText(c);
                answer_d.setText(dd);
            }
        }
    }

    private void set_color_a(){
        answer_a.setBackground(getResources().getDrawable(R.drawable.bg_button_outline_2));
        answer_a.setTextColor(getResources().getColor(android.R.color.white));
        answer_b.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_b.setTextColor(getResources().getColor(android.R.color.black));
        answer_c.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_c.setTextColor(getResources().getColor(android.R.color.black));
        answer_d.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_d.setTextColor(getResources().getColor(android.R.color.black));
    }

    private void set_color_b(){
        answer_a.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_a.setTextColor(getResources().getColor(android.R.color.black));
        answer_b.setBackground(getResources().getDrawable(R.drawable.bg_button_outline_2));
        answer_b.setTextColor(getResources().getColor(android.R.color.white));
        answer_c.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_c.setTextColor(getResources().getColor(android.R.color.black));
        answer_d.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_d.setTextColor(getResources().getColor(android.R.color.black));
    }

    private void set_color_c(){
        answer_a.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_a.setTextColor(getResources().getColor(android.R.color.black));
        answer_b.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_b.setTextColor(getResources().getColor(android.R.color.black));
        answer_c.setBackground(getResources().getDrawable(R.drawable.bg_button_outline_2));
        answer_c.setTextColor(getResources().getColor(android.R.color.white));
        answer_d.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_d.setTextColor(getResources().getColor(android.R.color.black));
    }

    private void set_color_d(){
        answer_a.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_a.setTextColor(getResources().getColor(android.R.color.black));
        answer_b.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_b.setTextColor(getResources().getColor(android.R.color.black));
        answer_c.setBackground(getResources().getDrawable(R.drawable.bg_button_outline));
        answer_c.setTextColor(getResources().getColor(android.R.color.black));
        answer_d.setBackground(getResources().getDrawable(R.drawable.bg_button_outline_2));
        answer_d.setTextColor(getResources().getColor(android.R.color.white));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        for (int i = 1; i<soalSize+1; i++){
            prefManager.setUserJawabanNull(i);
            prefManager.setUserJawabanBenarNull("BENAR");
            prefManager.setUserJawabanSalahNull("SALAH");
            prefManager.setJumlahSoalNull();
        }
    }
}
