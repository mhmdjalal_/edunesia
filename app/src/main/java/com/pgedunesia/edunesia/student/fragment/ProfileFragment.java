package com.pgedunesia.edunesia.student.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.BaseResponse;
import com.pgedunesia.edunesia.student.ui.login.LoginActivity;
import com.pgedunesia.edunesia.student.ui.profile.EditProfileActivity;
import com.pgedunesia.edunesia.student.viewmodel.ProfileViewModel;
import com.pgedunesia.edunesia.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.pgedunesia.edunesia.student.ui.profile.EditProfileActivity.TAG;

public class ProfileFragment extends Fragment {



    View view;

    @BindView(R.id.text_username)
    TextView text_name;
    @BindView(R.id.text_class)
    TextView text_class;
    @BindView(R.id.text_address)
    TextView text_address;
    @BindView(R.id.text_school)
    TextView text_school;
    @BindView(R.id.text_branch)
    TextView text_branch;
    @BindView(R.id.text_phone)
    TextView text_phone;
    @BindView(R.id.text_address_title)
    TextView text_address_title;
    @BindView(R.id.text_school_title)
    TextView text_school_title;
    @BindView(R.id.text_branch_title)
    TextView text_branch_title;
    @BindView(R.id.text_phone_title)
    TextView text_phone_title;
    @BindView(R.id.text_title)
    TextView text_title_info;
    @BindView(R.id.image_student)
    ImageView image_student;
    @BindView(R.id.button_keluar)
    Button btn_logout;
    @BindView(R.id.button_edit)
    Button btn_edit;

    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_GALERY = 2;
    private String currentPhotoPath;
    private File compressedImage;
    ApiInterface apiInterface;
    ProfileViewModel mViewModel;

    private String name_s, class_s, school_s, branch_s, phone_s, address_s;
    private int idsiswa;

    PrefManager prefManager;
    public static ProfileFragment newInstance(){
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = PrefManager.getInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        responsiveScreen();

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                101);

        init();

        loadData();

        return view;
    }

    private void loadData(){
        mViewModel.requestSiswa(prefManager.getSiswa().getId());
        mViewModel.getSiswa().observe(this, siswa -> {
            text_name.setText(siswa.getNama());
            text_address.setText(siswa.getAlamat());
            text_class.setText(siswa.getKelas());
            text_school.setText(siswa.getSekolah());
            text_branch.setText(siswa.getCabang());
            text_phone.setText(siswa.getNoTelepon());
            prefManager.setSiswa(siswa);
            CommonUtils.load_image(getContext(), image_student, siswa.getFoto());

        });
    }

    @OnClick(R.id.button_keluar)
    void onClickLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View v = getLayoutInflater().inflate(R.layout.dialog_konfirmasi_logout, null);
        TextView text_yes = v.findViewById(R.id.text_yes);
        TextView text_no = v.findViewById(R.id.text_no);
        builder.setView(v);

        AlertDialog alert = builder.create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        text_yes.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PrefManager.getInstance(getContext()).setIsLogin(false);
            startActivity(intent);
            getActivity().finish();
        });
        text_no.setOnClickListener(view -> alert.cancel());
        alert.show();
    }

    @OnClick(R.id.button_edit)
    void onClickEdit() {
        Intent intent = new Intent(getContext(), EditProfileActivity.class);
        intent.putExtra("siswa", prefManager.getSiswa());
        startActivity(intent);
    }

    private void init(){
        name_s = text_name.getText().toString();
        class_s = text_class.getText().toString();
        school_s = text_school.getText().toString();
        branch_s = text_branch.getText().toString();
        phone_s = text_phone.getText().toString();
        address_s = text_address.getText().toString();

        text_address.setSelected(true);

        image_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select_image();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO:
                    File file = new File(currentPhotoPath);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "JPEG_" + timeStamp + ".jpg";

                    try {
                        compressedImage = new Compressor(getActivity())
                                .setDestinationDirectoryPath(file.getParent() + "/")
                                .compressToFile(file, imageFileName);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    file.delete();

                    setPic(compressedImage);
                    Log.e("IMAGE : ", String.valueOf(compressedImage));
                    currentPhotoPath = compressedImage.getAbsolutePath();
                    prefManager.setUserImage(String.valueOf(compressedImage));
                    update_image(currentPhotoPath);
                    break;

                case REQUEST_GALERY:
                    Uri selectedImageUri = data.getData();
                    String picturePath = getPath(getActivity(), selectedImageUri);

                    File file2 = new File(picturePath);
                    String timeStamp2 = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName2 = "JPEG_" + timeStamp2 + ".jpg";

                    try {
                        compressedImage = new Compressor(getActivity())
                                .setDestinationDirectoryPath(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/")
                                .compressToFile(file2, imageFileName2);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    setPic(compressedImage);
                    currentPhotoPath = compressedImage.getAbsolutePath();
                    prefManager.setUserImage(String.valueOf(compressedImage));
                    update_image(currentPhotoPath);
                    break;
            }
        }
    }

    private void update_image(String currentPhotoPath){
        idsiswa = prefManager.getSiswa().getId();
        String foto = currentPhotoPath;
        File propertyImageFile = new File(foto);
        RequestBody propertyImage = RequestBody.create(MediaType.parse("image/*"), propertyImageFile);
        MultipartBody.Part fotoPart = MultipartBody.Part.createFormData("foto", propertyImageFile.getName(), propertyImage);

        apiInterface.updateGambar(idsiswa, fotoPart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.e("ERROR : ", t.getMessage());
            }
        });
    }

    private void select_image(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_select_photo, null, false);
        LinearLayout btnPicture = view.findViewById(R.id.btn_take_picture);
        LinearLayout btnFromGallery = view.findViewById(R.id.btn_from_gallery);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity())
                .setView(view);
        Dialog dialog = alertDialog.create();
        dialog.show();

        btnPicture.setOnClickListener(view1 -> {
            pickFromCamera();
            dialog.dismiss();
        });

        btnFromGallery.setOnClickListener(view12 -> {
            pickFromGallery();
            dialog.dismiss();
        });
    }

    private void pickFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.pgedunesia.edunesia.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        // Launching the Intent
        startActivityForResult(intent, REQUEST_GALERY);
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private File createImageFile() throws IOException {
        // Create an image file text_name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void setPic(File compressedImage) {
        Picasso.get()
                .load(compressedImage)
                .fit()
                .centerCrop()
                .placeholder(CommonUtils.progressDrawable(getActivity()))
                .into(image_student);
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.requestSiswa(prefManager.getSiswa().getId());
    }

    void responsiveScreen() {
        double screenSize = CommonUtils.getScreenDimension(getContext());
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            text_name.setTextSize(17f);
            text_class.setTextSize(14f);
            text_title_info.setTextSize(14f);
            text_school.setTextSize(12f);
            text_branch.setTextSize(12f);
            text_phone.setTextSize(12f);
            text_address.setTextSize(12f);
            text_school_title.setTextSize(11f);
            text_branch_title.setTextSize(11f);
            text_phone_title.setTextSize(11f);
            text_address_title.setTextSize(11f);
            btn_edit.setTextSize(14f);
            btn_logout.setTextSize(14f);
        } else {
            text_name.setTextSize(21f);
            text_class.setTextSize(17f);
            text_title_info.setTextSize(17f);
            text_school.setTextSize(15f);
            text_branch.setTextSize(15f);
            text_phone.setTextSize(15f);
            text_address.setTextSize(15f);
            text_school_title.setTextSize(14f);
            text_branch_title.setTextSize(14f);
            text_phone_title.setTextSize(14f);
            text_address_title.setTextSize(14f);
            btn_edit.setTextSize(17f);
            btn_logout.setTextSize(17f);
        }
    }
}
