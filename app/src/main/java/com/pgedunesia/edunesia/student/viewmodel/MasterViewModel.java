package com.pgedunesia.edunesia.student.viewmodel;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.data.response.StoreNilaiResponse;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.response.ListQuizResponse;
import com.pgedunesia.edunesia.data.model.Quiz;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.ActivationResponse;
import com.pgedunesia.edunesia.data.model.Mapel;
import com.pgedunesia.edunesia.data.response.MapelResponse;
import com.pgedunesia.edunesia.data.model.Ujian;
import com.pgedunesia.edunesia.data.response.UjianIdResponse;
import com.pgedunesia.edunesia.data.response.UjianResponse;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterViewModel extends AndroidViewModel {

    private MutableLiveData<List<Mapel>> mMapel = new MutableLiveData<>();
    private MutableLiveData<List<Ujian>> mUjian = new MutableLiveData<>();
    private MutableLiveData<Ujian> mUjianById = new MutableLiveData<>();
    private MutableLiveData<Quiz> mQuiz = new MutableLiveData<>();
    private MutableLiveData<List<Quiz>> mListQuiz = new MutableLiveData<>();

    ApiInterface apiInterface;

    public MasterViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
    }

    public LiveData<List<Mapel>> getmMapel() {
        return mMapel;
    }

    public LiveData<List<Ujian>> getmUjian(){
        return mUjian;
    }

    public LiveData<Ujian> getmUjianById() {
        return mUjianById;
    }

    public LiveData<Quiz> getQuiz() {
        return mQuiz;
    }

    public LiveData<List<Quiz>> getQuizzes() {
        return mListQuiz;
    }

    public void requestMapel() {
        apiInterface.getMapel()
                .enqueue(new Callback<MapelResponse>() {
                    @Override
                    public void onResponse(Call<MapelResponse> call, Response<MapelResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mMapel.postValue(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MapelResponse> call, Throwable t) {
                        Log.e("ERROR", t.getMessage());
                    }
                });
    }

    public void requestUjian(int id_siswa, int mapel_id){
        apiInterface.getUjian(id_siswa, mapel_id)
                .enqueue(new Callback<UjianResponse>() {
                    @Override
                    public void onResponse(Call<UjianResponse> call, Response<UjianResponse> response) {
                        if (response.isSuccessful()){
                            if (response.body() != null){
                                mUjian.postValue(response.body().getData());

                            }
                        }
                        Log.e("e", "Size" + response.body().getData().size());
                    }

                    @Override
                    public void onFailure(Call<UjianResponse> call, Throwable t) {
                        Log.e("ERROR", t.getMessage());
                    }
                });
    }

    public void checkAktivasi(int id_siswa, Class intent2, String id_ujian, String waktu){
        apiInterface.checkAktivasi(id_siswa)
                .enqueue(new Callback<ActivationResponse>() {
                    @Override
                    public void onResponse(Call<ActivationResponse> call, Response<ActivationResponse> response) {
                        if (response.isSuccessful()){
                            if (response.body() != null){
                                if (response.body().getAktif().equals("true")){
                                    Toast.makeText(getApplication(), "Akun anda telah diaktivasi, silahkan mengerjakan quiz...", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplication(), intent2);
                                    intent.putExtra("idUjian", id_ujian);
                                    intent.putExtra("time", waktu);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    getApplication().startActivity(intent);
                                }else{
                                    Toast.makeText(getApplication(), "Akun anda belum diaktivasi, tunggu beberapa saat lagi...", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Log.e("STATUS : ", "NULL");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ActivationResponse> call, Throwable t) {
                        Log.e("ERROR", t.getMessage());
                    }
                });
    }

    public void requestUjianByID(int id){
        apiInterface.getUjianById(id).enqueue(new Callback<UjianIdResponse>() {
            @Override
            public void onResponse(Call<UjianIdResponse> call, Response<UjianIdResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        mUjianById.postValue(response.body().getData());
                        Log.e("ERROR", response.body().getStatus());
                    }
                }
            }

            @Override
            public void onFailure(Call<UjianIdResponse> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
            }
        });
    }

    public void storeNilaiSiswa(int siswa_id, int ujian_id, int nilai){
        apiInterface.storeNilaiSiswa(siswa_id, ujian_id, nilai).enqueue(new Callback<StoreNilaiResponse>() {
            @Override
            public void onResponse(Call<StoreNilaiResponse> call, Response<StoreNilaiResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        Toast.makeText(getApplication(), "Berhasil Menyelesaikan Ujian", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<StoreNilaiResponse> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
            }
        });
    }

    public void requestQuizPopup() {
        apiInterface.getListQuiz(PrefManager.getInstance(getApplication()).getSiswa().getId())
                .enqueue(new Callback<ListQuizResponse>() {
                    @Override
                    public void onResponse(Call<ListQuizResponse> call, Response<ListQuizResponse> response) {
                        if (response.isSuccessful()) {
                            Quiz quiz;
                            List<Quiz> quizzes = new ArrayList<>();
                            if (response.body().isStatus()) {
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    if (response.body().getData().get(i).getStatus().equals("Belum Mengerjakan") && response.body().getData().get(i).getPublish().equals(CommonUtils.convertFormatDate("yyyy-MM-dd"))) {
                                        mQuiz.postValue(response.body().getData().get(i));
                                        quizzes.add(response.body().getData().get(i));
                                    }
                                }
                                mListQuiz.postValue(quizzes);
                            } else {
                                mListQuiz.postValue(quizzes);
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ListQuizResponse> call, Throwable t) {

                    }
                });
    }
}
