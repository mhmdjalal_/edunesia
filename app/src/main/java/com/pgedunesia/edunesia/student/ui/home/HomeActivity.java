package com.pgedunesia.edunesia.student.ui.home;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.data.model.Quiz;
import com.pgedunesia.edunesia.student.fragment.HomeFragment;
import com.pgedunesia.edunesia.student.fragment.ProfileFragment;
import com.pgedunesia.edunesia.student.fragment.ReportValueFragment;
import com.pgedunesia.edunesia.student.ui.quiz_popup.QuizPopupActivity;
import com.pgedunesia.edunesia.student.viewmodel.MasterViewModel;
import com.pgedunesia.edunesia.utils.CommonUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.nav_view)
    BottomNavigationView bottomNavigationView;

    MasterViewModel mViewModel;

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Menu menu = bottomNavigationView.getMenu();
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.ic_home_unselected);
            menu.findItem(R.id.navigation_score).setIcon(R.drawable.ic_trophy_unselected);
            menu.findItem(R.id.navigation_profile).setIcon(R.drawable.ic_profile_unselected);

            switch (menuItem.getItemId()) {
                case R.id.navigation_home:
                    menu.findItem(R.id.navigation_home).setIcon(R.drawable.ic_home_selected);
                    loadFragment(HomeFragment.newInstance());
                    return true;
                case R.id.navigation_score:
                    menu.findItem(R.id.navigation_score).setIcon(R.drawable.ic_trophy_selected);
                    loadFragment(ReportValueFragment.newInstance());
                    return true;
                case R.id.navigation_profile:
                    menu.findItem(R.id.navigation_profile).setIcon(R.drawable.ic_profile_selected);
                    loadFragment(ProfileFragment.newInstance());
                    return true;
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        responsiveScreen();

        mViewModel = ViewModelProviders.of(this).get(MasterViewModel.class);
        mViewModel.requestQuizPopup();

        mViewModel.getQuiz().observe(this, new Observer<Quiz>() {
            @Override
            public void onChanged(Quiz quiz) {
                if (quiz != null) {
                    show_quiz(quiz);
                }
            }
        });

        loadFragment(HomeFragment.newInstance());

        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
    }

    void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.frame_home, fragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void show_quiz(Quiz quiz){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View v = getLayoutInflater().inflate(R.layout.dialog_quiz_popup, null);
        builder.setView(v);

        TextView text_ujian = v.findViewById(R.id.text_ujian);
        TextView text_kelas = v.findViewById(R.id.text_kelas);
        TextView text_mapel = v.findViewById(R.id.text_mapel);
        TextView close = v.findViewById(R.id.text_close);
        ImageView image_quiz = v.findViewById(R.id.image_quiz);
        Button btnDetail = v.findViewById(R.id.button_detail);

        text_ujian.setText(quiz.getJudul());
        text_kelas.setText(quiz.getKelas());
        text_mapel.setText(quiz.getMapel());
        CommonUtils.load_image(this, image_quiz, quiz.getGambar());

        AlertDialog alert = builder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, QuizPopupActivity.class);
                intent.putExtra("quiz", (Serializable) quiz);
                startActivity(intent);
                alert.dismiss();
            }
        });
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, QuizPopupActivity.class);
                intent.putExtra("quiz", (Serializable) quiz);
                startActivity(intent);
                alert.dismiss();
            }
        });

        alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (!alert.isShowing()) {
            alert.show();
        } else {
            alert.dismiss();
        }
    }

    void responsiveScreen() {
        double screenSize = CommonUtils.getScreenDimension(this);
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            bottomNavigationView.setPadding(0,0,0,0);
            bottomNavigationView.setItemIconSize(24);
        } else {
            bottomNavigationView.setPadding(0,12,0,12);
            bottomNavigationView.setItemIconSize(35);
        }
    }
}
