package com.pgedunesia.edunesia.student.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefUjian;
import com.pgedunesia.edunesia.data.model.Soal;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PembahasanAdapter extends RecyclerView.Adapter<PembahasanAdapter.ViewHolder>{

    private Context context;
    private List<Soal> questionsList;
    private PrefUjian prefManager;
    private int jumlahBenar, jumlahSalah;
    private String pref_answer_benar, pref_answer_salah;

    public PembahasanAdapter(Context context) {
        this.context = context;
        prefManager = PrefUjian.getInstance(context);
    }

    public void updateAdapter(List<Soal> questionsList) {
        this.questionsList = questionsList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pembahasan, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Soal qt = questionsList.get(position);
        position = position + 1;

        String a = qt.getJawaban_a();
        a = "a";
        String b = qt.getJawaban_b();
        b = "b";
        String c = qt.getJawaban_c();
        c = "c";
        String d = qt.getJawaban_d();
        d = "d";
        String jawaban_benar = qt.getJawaban_benar();
        String jawaban_anda = prefManager.getSoalJawaban(String.valueOf(position));

        String no = String.valueOf(position);

        Spanned spanned_questions = Html.fromHtml(qt.getPertanyaan(), new ImageGetter(holder.questions), null);
        Spanned spanned_your_answer_a = Html.fromHtml(qt.getJawaban_a(), new ImageGetter(holder.your_answer), null);
        Spanned spanned_your_answer_b = Html.fromHtml(qt.getJawaban_b(), new ImageGetter(holder.your_answer), null);
        Spanned spanned_your_answer_c = Html.fromHtml(qt.getJawaban_c(), new ImageGetter(holder.your_answer), null);
        Spanned spanned_your_answer_d = Html.fromHtml(qt.getJawaban_d(), new ImageGetter(holder.your_answer), null);
        Spanned spanned_correct_answer_a = Html.fromHtml(qt.getJawaban_a(), new ImageGetter(holder.correct_answer), null);
        Spanned spanned_correct_answer_b = Html.fromHtml(qt.getJawaban_b(), new ImageGetter(holder.correct_answer), null);
        Spanned spanned_correct_answer_c = Html.fromHtml(qt.getJawaban_c(), new ImageGetter(holder.correct_answer), null);
        Spanned spanned_correct_answer_d = Html.fromHtml(qt.getJawaban_d(), new ImageGetter(holder.correct_answer), null);

        holder.text_number.setText(no + ". ");
        holder.questions.setText(spanned_questions);

        switch (jawaban_anda) {
            case "a":
                holder.text_answer.setText("( " + jawaban_anda.toUpperCase() + " ) ");
                holder.your_answer.setText(spanned_your_answer_a);
                break;
            case "b":
                holder.text_answer.setText("( " + jawaban_anda.toUpperCase() + " ) ");
                holder.your_answer.setText(spanned_your_answer_b);
                break;
            case "c":
                holder.text_answer.setText("( " + jawaban_anda.toUpperCase() + " ) ");
                holder.your_answer.setText(spanned_your_answer_c);
                break;
            case "d":
                holder.text_answer.setText("( " + jawaban_anda.toUpperCase() + " ) ");
                holder.your_answer.setText(spanned_your_answer_d);
                break;
            default:
                holder.your_answer.setText("Tidak ada jawaban");
                break;
        }

        if (jawaban_benar.equals(a)){
            holder.text_answer_correct.setText("( " + jawaban_benar.toUpperCase() + " ) ");
            holder.correct_answer.setText(spanned_correct_answer_a);
        }else if (jawaban_benar.equals(b)){
            holder.text_answer_correct.setText("( " + jawaban_benar.toUpperCase() + " ) ");
            holder.correct_answer.setText(spanned_correct_answer_b);
        }else if (jawaban_benar.equals(c)){
            holder.text_answer_correct.setText("( " + jawaban_benar.toUpperCase() + " ) ");
            holder.correct_answer.setText(spanned_correct_answer_c);
        }else if (jawaban_benar.equals(d)){
            holder.text_answer_correct.setText("( " + jawaban_benar.toUpperCase() + " ) ");
            holder.correct_answer.setText(spanned_correct_answer_d);
        }
        holder.pembahasan.setText(Html.fromHtml(qt.getPembahasan()));

        String benar = "benar";
        String salah = "salah";

        if (jawaban_benar.equals(jawaban_anda)){
            holder.status_image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_mark));
            holder.status.setText(benar.toUpperCase());
            //Log.e("NO. " + no,  "BENAR");
            jumlahBenar += 1;
            prefManager.setJawabanBenar("BENAR", String.valueOf(jumlahBenar));
        }else{
            holder.status_image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_cancel_mark));
            holder.status.setText(salah.toUpperCase());
            //Log.e("NO. " + no,  "SALAH");
            jumlahSalah += 1;
            prefManager.setJawabanSalah("SALAH", String.valueOf(jumlahSalah));
        }

        pref_answer_benar = prefManager.getJawabanBenar("BENAR");
        pref_answer_salah = prefManager.getJawabanSalah("SALAH");
        Log.e("JUMLAH JAWABAN BENAR", pref_answer_benar);
        Log.e("JUMLAH JAWABAN SALAH", pref_answer_salah);

    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_questions)
        TextView questions;
        @BindView(R.id.text_your_answer)
        TextView your_answer;
        @BindView(R.id.text_correct_answer)
        TextView correct_answer;
        @BindView(R.id.text_pembahasan)
        TextView pembahasan;
        @BindView(R.id.text_status_questions)
        TextView status;
        @BindView(R.id.image_status)
        ImageView status_image;
        @BindView(R.id.text_number)
        TextView text_number;
        @BindView(R.id.text_answer)
        TextView text_answer;
        @BindView(R.id.text_answer_correct)
        TextView text_answer_correct;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

    class ImageGetter implements Html.ImageGetter {

        TextView textView;
        ImageGetter(TextView textView) {
            this.textView = textView;
        }

        public Drawable getDrawable(String source) {
            LevelListDrawable d = new LevelListDrawable();
            Drawable empty = context.getResources().getDrawable(R.drawable.ic_launcher_foreground);
            d.addLevel(0, 0, empty);
            d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

            new LoadImage(textView).execute(source, d);

            return d;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        TextView textView;

        LoadImage(TextView textView) {
            this.textView = textView;
        }

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d("TAG", "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                Log.e("URL", source);
                return BitmapFactory.decodeStream(is);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("URL", Objects.requireNonNull(e.getMessage()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d("TAG", "onPostExecute drawable " + mDrawable);
            Log.d("TAG", "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected

                CharSequence qt = textView.getText();
                textView.setText(qt);
            }
        }
    }

}
