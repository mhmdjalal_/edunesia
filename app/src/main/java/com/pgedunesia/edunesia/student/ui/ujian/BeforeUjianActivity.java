package com.pgedunesia.edunesia.student.ui.ujian;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Soal;
import com.pgedunesia.edunesia.data.response.SoalResponse;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.student.ui.quiz.QuizActivity;
import com.pgedunesia.edunesia.student.viewmodel.MasterViewModel;
import com.pgedunesia.edunesia.student.viewmodel.ProfileViewModel;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeforeUjianActivity extends BaseActivity {

    @BindView(R.id.image_student)
    CircleImageView image_student;
    @BindView(R.id.text_name_user)
    TextView text_nama;
    @BindView(R.id.text_jenis_ujian)
    TextView text_jenis_ujian;
    @BindView(R.id.text_mapel_name)
    TextView text_mapel_name;
    @BindView(R.id.text_level_ujian)
    TextView text_level_ujian;
    @BindView(R.id.text_jumlah_soal)
    TextView text_jumlah_soal;
    @BindView(R.id.text_waktu_pengerjaan)
    TextView text_waktu_pengerjaan;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.button_mulai)
    Button button_mulai;

    String ujianId, ujianTime;
    int jumlahsoal;

    ProfileViewModel profileViewModel;
    MasterViewModel masterViewModel;
    PrefManager prefManager;
    ApiInterface apiInterface;
    public List<Soal> soalList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_before_ujian);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        masterViewModel = ViewModelProviders.of(this).get(MasterViewModel.class);
        prefManager = PrefManager.getInstance(this);
        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);

        load();
    }

    @SuppressLint("SetTextI18n")
    private void load(){
        Intent data = getIntent();
        if(data != null){
            ujianId = data.getStringExtra("idUjian");
            ujianTime = data.getStringExtra("time");
        }
        profileViewModel.requestSiswa(prefManager.getSiswa().getId());
        profileViewModel.getSiswa().observe(this, siswa -> {
            CommonUtils.load_image(BeforeUjianActivity.this, image_student, siswa.getFoto());
            text_nama.setText("Hai, " + siswa.getNama() + " !");

        });

        apiInterface.getSoal(Integer.parseInt(ujianId)).enqueue(new Callback<SoalResponse>() {
            @Override
            public void onResponse(Call<SoalResponse> call, Response<SoalResponse> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    soalList = response.body().getData();
                    jumlahsoal = soalList.size();
                    text_jumlah_soal.setText(jumlahsoal + " Soal");
                }
            }

            @Override
            public void onFailure(Call<SoalResponse> call, Throwable t) {
                Log.e("ERRORR", Objects.requireNonNull(t.getMessage()));
            }
        });

        masterViewModel.requestUjianByID(Integer.parseInt(ujianId));
        masterViewModel.getmUjianById().observe(this, ujian -> {
            text_jenis_ujian.setText(ujian.getNama());
            text_mapel_name.setText(ujian.getMapel());
            text_waktu_pengerjaan.setText(ujian.getWaktu() + " Menit");
            text_level_ujian.setText(ujian.getLevel().substring(0, 1).toUpperCase() + ujian.getLevel().substring(1));
        });

        button_mulai.setOnClickListener(view -> {
            Intent intent = new Intent(getApplication(), QuizActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("UJIAN_ID", ujianId);
            intent.putExtra("TIME", ujianTime);
            Log.e("UJIAN TIME", String.valueOf(ujianTime));
            getApplication().startActivity(intent);
        });

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
