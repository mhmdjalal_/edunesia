package com.pgedunesia.edunesia.student.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Ujian;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UjianAdapter extends RecyclerView.Adapter<UjianAdapter.ViewHolder> {

    private Context context;
    private List<Ujian> ujianList;
    final OnItemClickListener listener;

    public interface OnItemClickListener{
        void onItemClick(Ujian ujian);
    }

    public UjianAdapter(Context context, List<Ujian> ujianList, OnItemClickListener listener) {
        this.context = context;
        this.ujianList = ujianList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ujian, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Ujian ujian = ujianList.get(position);

        holder.ujian.setText(ujian.getNama());
        holder.waktu.setText(ujian.getWaktu() + " Menit");
        holder.mapel.setText(ujian.getMapel());
        holder.level.setText("( " + ujian.getLevel().substring(0, 1).toUpperCase() + ujian.getLevel().substring(1) +" )");

        if (ujian.getStatus().equals("Sudah mengerjakan")){
            holder.relativeLayout.setVisibility(View.VISIBLE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Ujian ini sudah anda kerjakan, silahkan pilih ujian lain...", Snackbar.LENGTH_LONG)
                            .show();
                }
            });
        }else{
            holder.relativeLayout.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(ujianList.get(position));
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return ujianList.size();
    }

    public void updateList(List<Ujian> ujians){
        this.ujianList = ujians;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_name_ujian)
        TextView ujian;
        @BindView(R.id.text_waktu_ujian)
        TextView waktu;
        @BindView(R.id.text_name_mapel)
        TextView mapel;
        @BindView(R.id.text_level_ujian)
        TextView level;
        @BindView(R.id.relative_ujian)
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
