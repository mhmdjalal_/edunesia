package com.pgedunesia.edunesia.student.ui.login;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.LoginSiswaResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginStudentViewModel extends AndroidViewModel {
    private MutableLiveData<LoginSiswaResponse> mSiswaResponse = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsLoading = new MutableLiveData<>();

    private ApiInterface apiInterface;

    public LoginStudentViewModel(@NonNull Application application) {
        super(application);
        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
    }

    public LiveData<LoginSiswaResponse> getLoginResponse() {
        return mSiswaResponse;
    }

    public LiveData<Boolean> isLoading() {
        return mIsLoading;
    }

    public void loginSiswa(String email, String password) {
        mIsLoading.postValue(true);
        apiInterface.loginSiswa(email, password)
                .enqueue(new Callback<LoginSiswaResponse>() {
                    @Override
                    public void onResponse(Call<LoginSiswaResponse> call, Response<LoginSiswaResponse> response) {
                        mIsLoading.postValue(false);
                        Log.d("isloading", "isloading1? " + mIsLoading.getValue());
                        Log.d("LOGIN_SISWA", "ok");
                        if (response.isSuccessful()) {
                            mSiswaResponse.postValue(response.body());
                        } else {
                            Log.d("LOGIN_SISWA", "response failed " + response.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginSiswaResponse> call, Throwable t) {
                        Log.d("LOGIN_SISWA", "bad " + t.getMessage());
                        mIsLoading.postValue(false);
                        Log.d("isloading", "isloading1? " + mIsLoading.getValue());
                    }
                });
    }

}
