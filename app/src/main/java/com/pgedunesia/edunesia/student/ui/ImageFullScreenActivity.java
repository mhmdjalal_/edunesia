package com.pgedunesia.edunesia.student.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.photodraweeview.PhotoDraweeView;

public class ImageFullScreenActivity extends AppCompatActivity {

    @BindView(R.id.image_quiz)
    PhotoDraweeView image_quiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_image_full_screen);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("image_quiz")) {
            image_quiz.setPhotoUri(Uri.parse(getIntent().getStringExtra("image_quiz")));
        } else if (getIntent().hasExtra("image_local")) {
            image_quiz.setImageBitmap(BitmapFactory.decodeFile(getIntent().getStringExtra("image_local")));
        }
    }

    @OnClick(R.id.image_close)
    void onClickButtonBack() {
        super.onBackPressed();
    }
}
