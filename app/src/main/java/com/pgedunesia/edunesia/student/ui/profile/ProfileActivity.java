package com.pgedunesia.edunesia.student.ui.profile;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Branch;
import com.pgedunesia.edunesia.data.model.Class;
import com.pgedunesia.edunesia.data.response.RegisterResponse;
import com.pgedunesia.edunesia.data.model.School;
import com.pgedunesia.edunesia.student.ui.home.HomeActivity;
import com.pgedunesia.edunesia.student.ui.search_branch.SelectBranchActivity;
import com.pgedunesia.edunesia.student.ui.search_class.SelectClassActivity;
import com.pgedunesia.edunesia.student.ui.search_school.SelectSchoolActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;
import com.pgedunesia.edunesia.utils.Utility;
import com.squareup.picasso.Picasso;
import com.pgedunesia.edunesia.student.viewmodel.ProfileViewModel;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import id.zelory.compressor.Compressor;

public class ProfileActivity extends BaseActivity {
    private static final String TAG = ProfileActivity.class.getSimpleName();
    private static final int REQUEST_STORAGE = 1;
    private static final int REQUEST_CAMERA = 2;
    private static final int REQUEST_SCHOOL = 3;
    private static final int REQUEST_CLASS = 4;
    private static final int REQUEST_BRANCH = 5;

    @BindView(R.id.text_class)
    TextView text_class;
    @BindView(R.id.text_school)
    TextView text_school;
    @BindView(R.id.text_branch)
    TextView text_branch;
    @BindView(R.id.edittext_nama)
    EditText etNama;
    @BindView(R.id.edittext_email)
    EditText etEmail;
    @BindView(R.id.edittext_phone)
    EditText etPhone;
    @BindView(R.id.edittext_alamat)
    EditText etAlamat;
    @BindView(R.id.view_camera)
    FrameLayout viewCamera;
    @BindView(R.id.profilePic)
    ImageView image_profile;

    ProfileViewModel mViewModel;
    File compressedImage;
    PrefManager prefManager;
    ProgressDialog progressDialog;

    School selectedSchool;
    Branch selectedBranch;
    Class selectedClass;

    String fileImagePath = "";
    String nama, email, phone, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        prefManager = PrefManager.getInstance(this);
        progressDialog = new ProgressDialog(this);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                101);

        if (getIntent().getExtras() != null) {
            nama = getIntent().getStringExtra("nama");
            email = getIntent().getStringExtra("email");
            phone = getIntent().getStringExtra("phone");
            password = getIntent().getStringExtra("password");
        }

        etNama.setText(nama);
        etEmail.setText(email);
        etPhone.setText(phone);
        Log.d("RegisterProfile", nama + " " + email + " " + phone + " " + password);

        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        mViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading != null) {
                    if (isLoading) {
                        progressDialog.setMessage("Loading...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }
        });
        mViewModel.getRegisterResponse().observe(this, new Observer<RegisterResponse>() {
            @Override
            public void onChanged(RegisterResponse response) {
                if (response != null) {
                    if (response.getStatus()) {
                        prefManager.setIsSiswa(true);
                        prefManager.setIsLogin(true);
                        prefManager.setSiswa(response.getData());
                        Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        if (response.getMessage() != null || !response.getMessage().isEmpty()) {
                            Toast.makeText(ProfileActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }

        viewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
    }

    @OnClick(R.id.text_school)
    public void onClickSelectSchool() {
        Intent intent = new Intent(this, SelectSchoolActivity.class);
        startActivityForResult(intent, REQUEST_SCHOOL);
    }

    @OnClick(R.id.text_branch)
    public void onClickSelectBranch() {
        Intent intent = new Intent(this, SelectBranchActivity.class);
        startActivityForResult(intent, REQUEST_BRANCH);
    }

    @OnClick(R.id.text_class)
    public void onClickSelectClass() {
        Intent intent = new Intent(this, SelectClassActivity.class);
        startActivityForResult(intent, REQUEST_CLASS);
    }

    @Optional
    @OnClick(R.id.image_back)
    public void onClickBack(View view) {
        onBackPressed();
    }

    @OnClick(R.id.button_next_profile)
    public void onClickNext() {
        if (selectedBranch == null) {
            Toast.makeText(this, "Silakan pilih cabang dahulu", Toast.LENGTH_SHORT).show();
        } else if (selectedClass == null) {
            Toast.makeText(this, "Silakan pilih kelas dahulu", Toast.LENGTH_SHORT).show();
        } else if (selectedSchool == null) {
            Toast.makeText(this, "Silakan pilih sekolah dahulu", Toast.LENGTH_SHORT).show();
        } else if (etEmail.getText().toString().isEmpty()) {
            Toast.makeText(this, "Silakan isi email dahulu", Toast.LENGTH_SHORT).show();
        } else if (etNama.getText().toString().isEmpty()) {
            Toast.makeText(this, "Silakan isi nama dahulu", Toast.LENGTH_SHORT).show();
        } else if (etAlamat.getText().toString().isEmpty()) {
            Toast.makeText(this, "Silakan isi alamat dahulu", Toast.LENGTH_SHORT).show();
        } else if (etPhone.getText().toString().isEmpty()) {
            Toast.makeText(this, "Silakan isi no telepon dahulu", Toast.LENGTH_SHORT).show();
        } else if (fileImagePath.isEmpty()) {
            Toast.makeText(this, "Silakan masukkan foto profil dahulu", Toast.LENGTH_SHORT).show();
        } else {
            mViewModel.registerUser(selectedBranch.getId(), selectedClass.getId(), selectedSchool.getId(), etEmail.getText().toString(), etNama.getText().toString(),
                    etAlamat.getText().toString(), etPhone.getText().toString(), password, fileImagePath);
        }
    }

    private void selectImage() {
        try {
            final CharSequence[] items = {"Kamera", "Galeri",
                    "Batal"};

            boolean result = Utility.checkPermission(this);
            boolean result2 = Utility.checkPermission2(this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Tambah foto profil");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {

                    if (items[item].equals("Kamera")) {
                        if (result2)
                            pickFromCamera();
                    } else if (items[item].equals("Galeri")) {

//                        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                            ActivityCompat.requestPermissions(ProfileActivity.this,
//                                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
//                                    1);
//                            return;
//                        } else {
                            pickFromGallery();
//                        }

                    } else if (items[item].equals("Batal")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
        }
    }

    private void pickFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.pgedunesia.edunesia.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        // Launching the Intent
        startActivityForResult(intent, REQUEST_STORAGE);
    }

    private void setPic(File compressedImage) {
        Picasso.get()
                .load(compressedImage)
                .fit()
                .centerCrop()
                .placeholder(CommonUtils.progressDrawable(this))
                .into(image_profile);
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private File createImageFile() throws IOException {
        // Create an image compressedImage name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a compressedImage: path for use with ACTION_VIEW intents
        fileImagePath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    File file = new File(fileImagePath);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "JPEG_" + timeStamp + ".jpg";

                    try {
                        compressedImage = new Compressor(this)
                                .setDestinationDirectoryPath(file.getParent() + "/")
                                .compressToFile(file, imageFileName);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    file.delete();

                    setPic(compressedImage);
                    Log.e("IMAGE : ", String.valueOf(compressedImage));
                    fileImagePath = compressedImage.getAbsolutePath();
                    break;

                case REQUEST_STORAGE:
                    Uri selectedImageUri = data.getData();
                    String picturePath = getPath(this, selectedImageUri);

                    File file2 = new File(picturePath);
                    String timeStamp2 = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName2 = "JPEG_" + timeStamp2 + ".jpg";

                    try {
                        compressedImage = new Compressor(this)
                                .setDestinationDirectoryPath(getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/")
                                .compressToFile(file2, imageFileName2);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    setPic(compressedImage);
                    fileImagePath = compressedImage.getAbsolutePath();
                    break;
                case REQUEST_SCHOOL:
                    selectedSchool = (School) data.getSerializableExtra("school");
                    text_school.setText(selectedSchool.getNama());
                    break;
                case REQUEST_BRANCH:
                    selectedBranch = (Branch) data.getSerializableExtra("branch");
                    text_branch.setText(selectedBranch.getNama());
                    break;
                case REQUEST_CLASS:
                    selectedClass = (Class) data.getSerializableExtra("class");
                    text_class.setText(selectedClass.getKelas());
                    break;
            }
        }
    }


}
