package com.pgedunesia.edunesia.student.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.data.model.Class;
import com.pgedunesia.edunesia.data.model.Student;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.BaseResponse;
import com.pgedunesia.edunesia.data.response.RegisterResponse;
import com.pgedunesia.edunesia.data.model.Branch;
import com.pgedunesia.edunesia.data.response.BranchResponse;
import com.pgedunesia.edunesia.data.response.ClassResponse;
import com.pgedunesia.edunesia.data.model.School;
import com.pgedunesia.edunesia.data.response.SchoolResponse;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileViewModel extends AndroidViewModel {
    private MutableLiveData<List<Branch>> mCabang = new MutableLiveData<>();
    private MutableLiveData<List<School>> mSekolah = new MutableLiveData<>();
    private MutableLiveData<List<Class>> mKelas = new MutableLiveData<>();
    private MutableLiveData<List<Student>> mSiswa = new MutableLiveData<>();
    private MutableLiveData<Student> siswa = new MutableLiveData<>();
    private MutableLiveData<RegisterResponse> mRegisterResponse = new MutableLiveData<>();
    private MutableLiveData<BaseResponse> mUpdateResponse = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsLoading = new MutableLiveData<>();

    ApiInterface apiInterface;

    public ProfileViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);

        requestCabang();
        requestKelas();
        requestSekolah();
    }

    public LiveData<List<Branch>> getCabangs() {
        return mCabang;
    }

    public LiveData<List<School>> getSekolahs() {
        return mSekolah;
    }

    public LiveData<List<Class>> getKelases() {
        return mKelas;
    }

    public LiveData<List<Student>> getmSiswa() {
        return mSiswa;
    }

    public LiveData<Student> getSiswa() {
        return siswa;
    }

    public LiveData<RegisterResponse> getRegisterResponse() {
        return mRegisterResponse;
    }

    public LiveData<BaseResponse> getUpdateResponse() {
        return mUpdateResponse;
    }

    public LiveData<Boolean> isLoading() {
        return mIsLoading;
    }

    public void requestCabang() {
        mIsLoading.postValue(true);
        apiInterface.getAllCabang()
                .enqueue(new Callback<BranchResponse>() {
                    @Override
                    public void onResponse(Call<BranchResponse> call, Response<BranchResponse> response) {
                        mIsLoading.postValue(false);
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mCabang.postValue(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BranchResponse> call, Throwable t) {
                        mIsLoading.postValue(false);
                    }
                });
    }

    public void requestSekolah() {
        mIsLoading.postValue(true);
        apiInterface.getAllSekolah()
                .enqueue(new Callback<SchoolResponse>() {
                    @Override
                    public void onResponse(Call<SchoolResponse> call, Response<SchoolResponse> response) {
                        mIsLoading.postValue(false);
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mSekolah.postValue(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SchoolResponse> call, Throwable t) {
                        mIsLoading.postValue(false);
                    }
                });
    }

    public void requestKelas() {
        mIsLoading.postValue(true);
        apiInterface.getAllKelas()
                .enqueue(new Callback<ClassResponse>() {
                    @Override
                    public void onResponse(Call<ClassResponse> call, Response<ClassResponse> response) {
                        mIsLoading.postValue(false);
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mKelas.postValue(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ClassResponse> call, Throwable t) {
                        mIsLoading.postValue(false);
                    }
                });
    }

    public void registerUser(int cabang_id, int kelas_id, int sekolah_id, String email, String nama, String alamat, String phone, String password, String photoPath) {
        mIsLoading.postValue(true);
        File imageFile = new File(photoPath);
        RequestBody propertyImage = RequestBody.create(MediaType.parse("image/*"), imageFile);
        MultipartBody.Part file = MultipartBody.Part.createFormData("foto", imageFile.getName(), propertyImage);

        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("cabang_id", CommonUtils.createPartFromString(String.valueOf(cabang_id)));
        body.put("kelas_id", CommonUtils.createPartFromString(String.valueOf(kelas_id)));
        body.put("sekolah_id", CommonUtils.createPartFromString(String.valueOf(sekolah_id)));
        body.put("email", CommonUtils.createPartFromString(email));
        body.put("nama", CommonUtils.createPartFromString(nama));
        body.put("alamat", CommonUtils.createPartFromString(alamat));
        body.put("no_telepon", CommonUtils.createPartFromString(phone));
        body.put("password", CommonUtils.createPartFromString(password));

        apiInterface.registerSiswa(file, body)
                .enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                        mIsLoading.postValue(false);
                        if (response.isSuccessful()) {
                            mRegisterResponse.postValue(response.body());
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        mIsLoading.postValue(false);
                        Log.e("REGISTER", "onFailure " +  t.getMessage());
                    }
                });
    }

    public void updateProfile(int id, int cabang_id, int sekolah_id, int kelas_id, String nama, String alamat, String no_telepon) {
        mIsLoading.postValue(true);
        apiInterface.updateProfilSiswa(id, cabang_id, sekolah_id, kelas_id, nama, alamat, no_telepon)
                .enqueue(new Callback<BaseResponse>() {

                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        mIsLoading.postValue(false);
                        if (response.isSuccessful()){
                            mUpdateResponse.postValue(response.body());
//                            if (response.body() != null){
//                                Toast.makeText(getApplication(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                            }else{
//                                Toast.makeText(getApplication(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        mIsLoading.postValue(false);
                        Log.e("ERROR : ", t.getMessage());
                    }
                });
    }

    public void requestSiswa(int id){
        apiInterface.showSiswa(id).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        siswa.postValue(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e("ERROR : ", t.getMessage());
            }
        });
    }
}
