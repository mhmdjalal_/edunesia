package com.pgedunesia.edunesia.student.ui.register;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.data.model.Class;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.ClassResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterViewModel extends AndroidViewModel {
    private MutableLiveData<List<Class>> mResponseKelas = new MutableLiveData<>();


    ApiInterface apiInterface;

    public RegisterViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        requestKelas();
    }

    public LiveData<List<Class>> getResponseKelas() {
        return mResponseKelas;
    }

    public void requestKelas() {
        apiInterface.getAllKelas()
                .enqueue(new Callback<ClassResponse>() {
                    @Override
                    public void onResponse(Call<ClassResponse> call, Response<ClassResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mResponseKelas.postValue(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ClassResponse> call, Throwable t) {

                    }
                });
    }
}
