package com.pgedunesia.edunesia.student.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.AppConfig;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Quiz;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.student.adapter.MainMapelAdapter;
import com.pgedunesia.edunesia.data.model.Mapel;
import com.pgedunesia.edunesia.student.adapter.QuizAdapter;
import com.pgedunesia.edunesia.student.ui.list_mapel.ListMapelActivity;
import com.pgedunesia.edunesia.student.ui.notification.StudentNotifActivity;
import com.pgedunesia.edunesia.student.viewmodel.MasterViewModel;
import com.pgedunesia.edunesia.student.viewmodel.ProfileViewModel;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends Fragment {

    @BindView(R.id.image_notification)
    ImageView image_notification;
    @BindView(R.id.recycler_mapel)
    RecyclerView recycler_mapel;
    @BindView(R.id.progress_mapel)
    ProgressBar progress_mapel;
    @BindView(R.id.progress_quiz)
    ProgressBar progress_quiz;
    @BindView(R.id.recycler_quiz)
    RecyclerView recycler_quiz;
    @BindView(R.id.view_quiz_zero)
    LinearLayout view_no_quiz;
    @BindView(R.id.text_welcome)
    TextView text_welcome;
    @BindView(R.id.text_greeting)
    TextView text_greeting;

    PrefManager prefManager;
    ApiInterface apiInterface;

    QuizAdapter quizAdapter;
    private MainMapelAdapter adapter;
    private View view;

    MasterViewModel mViewModel;
    ProfileViewModel profileViewModel;

    public static HomeFragment newInstance(){
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = PrefManager.getInstance(getContext());
        adapter = new MainMapelAdapter(getContext());
        quizAdapter = new QuizAdapter(getContext());
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        responsiveScreen();

        progress_mapel.setVisibility(View.VISIBLE);
        progress_quiz.setVisibility(View.VISIBLE);
        mViewModel = ViewModelProviders.of(this).get(MasterViewModel.class);
        mViewModel.requestQuizPopup();
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        load();
        show_questions_dialog();

        GridLayoutManager manager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
        recycler_mapel.setLayoutManager(manager);
        recycler_mapel.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recycler_quiz.setLayoutManager(linearLayoutManager);
        recycler_quiz.setAdapter(quizAdapter);

        load_time();
        return view;
    }

    @OnClick(R.id.image_notification)
    void onClickNotification() {
        startActivity(new Intent(getContext(), StudentNotifActivity.class));
    }

    @OnClick(R.id.view_more_mapel)
    void onClickViewMoreMapel() {
        startActivity(new Intent(getContext(), ListMapelActivity.class));
    }

    private void load_time(){
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if(timeOfDay >= 0 && timeOfDay < 12){
            text_welcome.setText(Html.fromHtml("<b>Hello</b>, Selamat Pagi<br><b>Fly Beyond Expectations</b>"));
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            text_welcome.setText(Html.fromHtml("<b>Hello</b>, Selamat Siang<br><b>Fly Beyond Expectations</b>"));
        }else if(timeOfDay >= 16 && timeOfDay < 21){
            text_welcome.setText(Html.fromHtml("<b>Hello</b>, Selamat Sore<br><b>Fly Beyond Expectations</b>"));
        }else if(timeOfDay >= 21 && timeOfDay < 24){
            text_welcome.setText(Html.fromHtml("<b>Hello</b>, Selamat Malam<br><b>Fly Beyond Expectations</b>"));
        }
    }

    private void load(){
        mViewModel.requestMapel();
        mViewModel.getmMapel().observe(this, new Observer<List<Mapel>>() {
            @Override
            public void onChanged(List<Mapel> mapels) {
                if (mapels != null) {
                    progress_mapel.setVisibility(View.GONE);
                    adapter.updateData(mapels);
                }
            }
        });

        mViewModel.getQuizzes().observe(this, new Observer<List<Quiz>>() {
            @Override
            public void onChanged(List<Quiz> quizzes) {
                if (quizzes != null) {
                    progress_quiz.setVisibility(View.GONE);
                    if (quizzes.size() > 0) {
                        recycler_quiz.setVisibility(View.VISIBLE);
                        view_no_quiz.setVisibility(View.GONE);
                        quizAdapter.updateData(quizzes);
                    } else {
                        view_no_quiz.setVisibility(View.VISIBLE);
                        recycler_quiz.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.requestQuizPopup();
    }

    private void show_questions_dialog(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(getContext(), "Dialog Show", Toast.LENGTH_SHORT).show();
            }
        }, AppConfig.DELAY_DIALOG);
    }

    void responsiveScreen() {
        double screenSize = CommonUtils.getScreenDimension(getContext());
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            text_welcome.setTextSize(17f);
            text_greeting.setTextSize(13f);
        } else {
            text_welcome.setTextSize(21f);
            text_greeting.setTextSize(15f);
        }
    }
}
