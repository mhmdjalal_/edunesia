package com.pgedunesia.edunesia.student.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.data.model.Class;
import com.pgedunesia.edunesia.student.ui.profile.ProfileActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.edittext_nama)
    EditText etNama;
    @BindView(R.id.edittext_email)
    EditText etEmail;
    @BindView(R.id.edittext_phone)
    EditText etPhone;
    @BindView(R.id.edittext_password)
    EditText etPassword;
    @BindView(R.id.header)
    FrameLayout header;
    @BindView(R.id.bottom_section)
    LinearLayout bottom_section;

    RegisterViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        responsiveScreen();

        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);

        mViewModel.getResponseKelas().observe(this, new Observer<List<Class>>() {
            @Override
            public void onChanged(List<Class> kelases) {
                if (kelases != null) {
//                    spinner_kelas.setAdapter(new ClassSpinnerAdapter(RegisterActivity.this, R.layout.item_selection, kelases));
                }
            }
        });

        loadAnimation();
    }

    void loadAnimation() {
        Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        bottom_section.startAnimation(slideUp);
        Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        header.startAnimation(slideDown);
    }

    @OnClick(R.id.image_back)
    public void onClickBack(View view) {
        onBackPressed();
    }

    @OnClick(R.id.button_register)
    public void onClickNext(View view){
        String nama = etNama.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (nama.isEmpty()) {
            etNama.requestFocus();
        } else if (phone.isEmpty()) {
            etPhone.requestFocus();
        } else if (email.isEmpty()) {
            etEmail.requestFocus();
        } else if (password.isEmpty()) {
            etPassword.requestFocus();
        } else {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("nama", nama);
            intent.putExtra("phone", phone);
            intent.putExtra("email", email);
            intent.putExtra("password", password);
            startActivity(intent);
        }
    }

    void responsiveScreen() {
        double screenSize = CommonUtils.getScreenDimension(this);
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
            header.setLayoutParams(params);
        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 380);
            header.setLayoutParams(params);
        }
    }
}
