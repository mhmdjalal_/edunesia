package com.pgedunesia.edunesia.student.ui.list_mapel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.material.appbar.AppBarLayout;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Mapel;
import com.pgedunesia.edunesia.student.adapter.MapelAdapter;
import com.pgedunesia.edunesia.student.viewmodel.MasterViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListMapelActivity extends AppCompatActivity implements NestedScrollView.OnScrollChangeListener, SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.recycler_mapel)
    RecyclerView recycler_mapel;
    @BindView(R.id.nestedScroll)
    NestedScrollView nestedScrollView;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.progress_mapel)
    ProgressBar progressBar;
    @BindView(R.id.search_view)
    SearchView searchView;

    MasterViewModel mViewModel;
    MapelAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_mapel);
        ButterKnife.bind(this);

        adapter = new MapelAdapter(this);

        progressBar.setVisibility(View.VISIBLE);
        mViewModel = ViewModelProviders.of(this).get(MasterViewModel.class);
        mViewModel.requestMapel();
        mViewModel.getmMapel().observe(this, new Observer<List<Mapel>>() {
            @Override
            public void onChanged(List<Mapel> mapels) {
                if (mapels != null) {
                    if (mapels.size() > 0) {
                        adapter.updateData(mapels);
                    } else {

                    }
                }
                progressBar.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });

        recycler_mapel.setLayoutManager(new LinearLayoutManager(this));
        recycler_mapel.setAdapter(adapter);

        nestedScrollView.setOnScrollChangeListener(this);
        swipeRefresh.setOnRefreshListener(this);
        searchView.setOnQueryTextListener(this);
    }

    @OnClick(R.id.image_back)
    void onClickBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (scrollY > oldScrollY) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(5f);
            }
        }
        if (scrollY < oldScrollY && scrollY == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(0f);
            }
        }
    }

    @Override
    public void onRefresh() {
        swipeRefresh.setRefreshing(true);
        mViewModel.requestMapel();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }
}
