package com.pgedunesia.edunesia.student.ui.quiz_popup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.card.MaterialCardView;
import com.onurkagan.ksnack_lib.Animations.Slide;
import com.onurkagan.ksnack_lib.KSnack.KSnack;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.response.BaseResponse;
import com.pgedunesia.edunesia.data.model.Quiz;
import com.pgedunesia.edunesia.student.ui.ImageFullScreenActivity;
import com.pgedunesia.edunesia.student.viewmodel.QuizPopupViewModel;
import com.pgedunesia.edunesia.utils.CommonUtils;
import com.pgedunesia.edunesia.utils.Utility;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

public class QuizPopupActivity extends AppCompatActivity implements NestedScrollView.OnScrollChangeListener {
    private static final int REQUEST_STORAGE = 1;
    private static final int REQUEST_CAMERA = 2;
    private static final String TAG = QuizPopupActivity.class.getSimpleName();

    @BindView(R.id.text_ujian)
    TextView text_judul;
    @BindView(R.id.text_kelas)
    TextView text_kelas;
    @BindView(R.id.text_mapel)
    TextView text_mapel;
    @BindView(R.id.text_upload)
    TextView text_upload;
    @BindView(R.id.image_quiz)
    ImageView image_quiz;
    @BindView(R.id.image_jawaban)
    ImageView image_jawaban;
    @BindView(R.id.image_placeholder)
    ImageView image_placeholder;
    @BindView(R.id.upload_jawaban)
    MaterialCardView upload_jawaban;
    @BindView(R.id.nestedScroll)
    NestedScrollView nestedScrollView;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;

    QuizPopupViewModel mViewModel;

    File compressedImage;
    ProgressDialog progressDialog;

    String fileImagePath = "";
    Quiz quiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_popup);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        mViewModel = ViewModelProviders.of(this).get(QuizPopupViewModel.class);
        mViewModel.getSendResponse().observe(this, new Observer<BaseResponse>() {
            @Override
            public void onChanged(BaseResponse baseResponse) {
                mViewModel.setIsLoading(false);
                if (baseResponse != null) {
                    if (baseResponse.getStatus()) {
                        KSnack kSnack = new KSnack(QuizPopupActivity.this);
                        kSnack.setMessage(baseResponse.getMessage())
                                .setTextColor(android.R.color.white)
                                .setBackgrounDrawable(R.drawable.bg_popup_white)
                                .setBackColor(R.color.gradientLightGreen)
                                .setAnimation(Slide.Up.getAnimation(kSnack.getSnackView()), Slide.Down.getAnimation(kSnack.getSnackView()))
                                .setDuration(2000)
                                .show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                QuizPopupActivity.super.onBackPressed();
                            }
                        }, 2100);
                    } else {
                        KSnack kSnack = new KSnack(QuizPopupActivity.this);
                        kSnack.setMessage(baseResponse.getMessage())
                                .setTextColor(android.R.color.white)
                                .setBackgrounDrawable(R.drawable.bg_popup_white)
                                .setBackColor(R.color.colorRed)
                                .setAnimation(Slide.Up.getAnimation(kSnack.getSnackView()), Slide.Down.getAnimation(kSnack.getSnackView()))
                                .setDuration(2000)
                                .show();
                    }
                }
            }
        });
        mViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading != null) {
                    if (isLoading) {
                        progressDialog.setMessage("Loading...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }
        });

        if (getIntent().hasExtra("quiz")) {
            quiz = (Quiz) getIntent().getSerializableExtra("quiz");
        }

        text_judul.setText(quiz.getJudul());
        text_kelas.setText(quiz.getKelas());
        text_mapel.setText(quiz.getMapel());
        CommonUtils.load_image(this, image_quiz, quiz.getGambar());

        upload_jawaban.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (!fileImagePath.isEmpty()) {
                    Intent intent = new Intent(QuizPopupActivity.this, ImageFullScreenActivity.class);
                    intent.putExtra("image_local", fileImagePath);
                    startActivity(intent);
                } else {
                    selectImage();
                }
                return false;
            }
        });

        nestedScrollView.setOnScrollChangeListener(this);
    }

    @OnClick(R.id.image_back)
    void onClickImageBack() {
        super.onBackPressed();
    }

    @OnClick(R.id.image_quiz)
    void onClickImageSoal() {
        Intent intent = new Intent(this, ImageFullScreenActivity.class);
        intent.putExtra("image_quiz", quiz.getGambar());
        startActivity(intent);
    }

    @OnClick(R.id.upload_jawaban)
    void onClickImageJawaban() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                101);

        selectImage();
    }

    @OnClick(R.id.button_send)
    void onClickSendJawaban() {
        if (!fileImagePath.equals("")) {
            mViewModel.setIsLoading(true);
            mViewModel.sendJawabanQuiz(quiz.getId(), fileImagePath);
        } else {
            KSnack kSnack = new KSnack(this);
            kSnack.setMessage("Masukan jawaban kamu")
                    .setTextColor(android.R.color.white)
                    .setBackgrounDrawable(R.drawable.bg_popup_white)
                    .setBackColor(R.color.colorRed)
                    .setAnimation(Slide.Up.getAnimation(kSnack.getSnackView()), Slide.Down.getAnimation(kSnack.getSnackView()))
                    .setDuration(2000)
                    .show();
        }
    }

    private void selectImage() {
        try {
            final CharSequence[] items = {"Kamera", "Galeri",
                    "Batal"};

            boolean result = Utility.checkPermission(this);
            boolean result2 = Utility.checkPermission2(this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Upload jawaban");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {

                    if (items[item].equals("Kamera")) {
                        if (result2)
                            pickFromCamera();
                    } else if (items[item].equals("Galeri")) {
                        pickFromGallery();
                    } else if (items[item].equals("Batal")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
        }
    }

    private void pickFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.pgedunesia.edunesia.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        // Launching the Intent
        startActivityForResult(intent, REQUEST_STORAGE);
    }

    private void setPic(File compressedImage) {
        Picasso.get()
                .load(compressedImage)
                .fit()
                .centerCrop()
                .placeholder(CommonUtils.progressDrawable(this))
                .into(image_jawaban);
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private File createImageFile() throws IOException {
        // Create an image compressedImage name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a compressedImage: path for use with ACTION_VIEW intents
        fileImagePath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            image_placeholder.setVisibility(View.GONE);
            text_upload.setVisibility(View.GONE);
            switch (requestCode) {
                case REQUEST_CAMERA:
                    File file = new File(fileImagePath);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "JPEG_" + timeStamp + ".jpg";

                    try {
                        compressedImage = new Compressor(this)
                                .setDestinationDirectoryPath(file.getParent() + "/")
                                .compressToFile(file, imageFileName);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    file.delete();

                    setPic(compressedImage);
                    Log.e("IMAGE : ", String.valueOf(compressedImage));
                    fileImagePath = compressedImage.getAbsolutePath();
                    break;

                case REQUEST_STORAGE:
                    Uri selectedImageUri = data.getData();
                    String picturePath = getPath(this, selectedImageUri);

                    File file2 = new File(picturePath);
                    String timeStamp2 = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName2 = "JPEG_" + timeStamp2 + ".jpg";

                    try {
                        compressedImage = new Compressor(this)
                                .setDestinationDirectoryPath(getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/")
                                .compressToFile(file2, imageFileName2);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }

                    setPic(compressedImage);
                    fileImagePath = compressedImage.getAbsolutePath();
                    break;
            }
        }
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (scrollY > oldScrollY) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(5f);
            }
        }
        if (scrollY < oldScrollY && scrollY == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(0f);
            }
        }
    }
}
