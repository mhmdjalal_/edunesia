package com.pgedunesia.edunesia.student.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.response.NilaiQuizResponse;
import com.pgedunesia.edunesia.data.model.Quiz;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.model.NilaiUjian;
import com.pgedunesia.edunesia.data.response.ReportNilaiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeStudentViewModel extends AndroidViewModel {
    private MutableLiveData<List<NilaiUjian>> mNilaiUjian = new MutableLiveData<>();
    private MutableLiveData<List<Quiz>> mNilaiQuiz = new MutableLiveData<>();
    private MutableLiveData<NilaiQuizResponse> quizResponse = new MutableLiveData<>();
    private MutableLiveData<ReportNilaiResponse> ujianResponse = new MutableLiveData<>();

    ApiInterface apiInterface;
    PrefManager prefManager;

    public HomeStudentViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        prefManager = PrefManager.getInstance(getApplication());
    }

    public LiveData<List<NilaiUjian>> getNilaiUjian() {
        return mNilaiUjian;
    }

    public LiveData<List<Quiz>> getNilaiQuiz() {
        return mNilaiQuiz;
    }

    public LiveData<NilaiQuizResponse> getQuizResponse() {
        return quizResponse;
    }

    public LiveData<ReportNilaiResponse> getUjianResponse() {
        return ujianResponse;
    }

    public void loadNilaiUjian(int siswa_id) {
        apiInterface.getNilaiSiswa(siswa_id)
                .enqueue(new Callback<ReportNilaiResponse>() {
                    @Override
                    public void onResponse(Call<ReportNilaiResponse> call, Response<ReportNilaiResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()) {
                                ujianResponse.postValue(response.body());
                                mNilaiUjian.postValue(response.body().getData());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ReportNilaiResponse> call, Throwable t) {

                    }
                });
    }

    public void loadNilaiQuiz(int siswa_id) {
        apiInterface.getNilaiQuiz(siswa_id)
                .enqueue(new Callback<NilaiQuizResponse>() {
                    @Override
                    public void onResponse(Call<NilaiQuizResponse> call, Response<NilaiQuizResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().isStatus()) {
                                quizResponse.postValue(response.body());
                                mNilaiQuiz.postValue(response.body().getData());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<NilaiQuizResponse> call, Throwable t) {

                    }
                });
    }
}
