package com.pgedunesia.edunesia.student.ui.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.AppBarLayout;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.response.BaseResponse;
import com.pgedunesia.edunesia.data.model.Class;
import com.pgedunesia.edunesia.data.model.Branch;
import com.pgedunesia.edunesia.data.model.School;
import com.pgedunesia.edunesia.student.ui.search_branch.SelectBranchActivity;
import com.pgedunesia.edunesia.student.ui.search_class.SelectClassActivity;
import com.pgedunesia.edunesia.student.ui.search_school.SelectSchoolActivity;
import com.pgedunesia.edunesia.student.viewmodel.ProfileViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditProfileActivity extends BaseActivity implements NestedScrollView.OnScrollChangeListener{

    public static final String TAG = EditProfileActivity.class.getSimpleName();
    private static final int REQUEST_SCHOOL = 3;
    private static final int REQUEST_CLASS = 4;
    private static final int REQUEST_BRANCH = 5;

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.text_class)
    TextView text_class;
    @BindView(R.id.text_school)
    TextView text_school;
    @BindView(R.id.text_branch)
    TextView text_branch;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.button_ubah)
    Button btn_ubah;
    @BindView(R.id.nestedScroll)
    NestedScrollView nestedScrollView;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;

    ProfileViewModel mViewModel;
    PrefManager prefManager;
    ProgressDialog progressDialog;

    School selectedSchool;
    Branch selectedBranch;
    Class selectedClass;

    private int branchId, schoolId, classId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        prefManager = PrefManager.getInstance(this);
        progressDialog = new ProgressDialog(this);

        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        mViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading != null) {
                    Log.d("isloading", "isloading2? " + isLoading);
                    if (isLoading) {
                        progressDialog.setMessage("Loading...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }
        });
        mViewModel.getUpdateResponse().observe(this, new Observer<BaseResponse>() {
            @Override
            public void onChanged(BaseResponse response) {
                if (response != null) {
                    if (response.getStatus()) {
//                        prefManager.setSiswa(response.getData());
//                        Intent intent = new Intent();
//                        intent.putExtra("page_position", 3);
//                        setResult(RESULT_OK, intent);
                        Toast.makeText(EditProfileActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    } else {
                        if (response.getMessage() != null || !response.getMessage().isEmpty()) {
                            Toast.makeText(EditProfileActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        init();
    }

    @OnClick(R.id.text_school)
    public void onClickSelectSchool() {
        Intent intent = new Intent(this, SelectSchoolActivity.class);
        startActivityForResult(intent, REQUEST_SCHOOL);
    }

    @OnClick(R.id.text_branch)
    public void onClickSelectBranch() {
        Intent intent = new Intent(this, SelectBranchActivity.class);
        startActivityForResult(intent, REQUEST_BRANCH);
    }

    @OnClick(R.id.text_class)
    public void onClickSelectClass() {
        Intent intent = new Intent(this, SelectClassActivity.class);
        startActivityForResult(intent, REQUEST_CLASS);
    }

    @OnClick(R.id.image_back)
    void onClickBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.button_ubah)
    void onClickButtonUbah() {
        update_siswa();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {
        mViewModel.requestSiswa(prefManager.getSiswa().getId());
        mViewModel.getSiswa().observe(this, siswa -> {
            etName.setText(siswa.getNama());
            etPhone.setText(siswa.getNoTelepon());
            etAddress.setText(siswa.getAlamat());

            text_branch.setText(siswa.getCabang());
            text_school.setText(siswa.getSekolah());
            text_class.setText(siswa.getKelas());

            classId = siswa.getKelasId();
            schoolId = siswa.getSekolahId();
            branchId = siswa.getCabangId();
        });

    }

    private void update_siswa(){
        int id = prefManager.getSiswa().getId();
        String nama = etName.getText().toString();
        String alamat = etAddress.getText().toString();
        String no_hp = etPhone.getText().toString();

        mViewModel.updateProfile(id, branchId, schoolId, classId, nama, alamat, no_hp);
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (scrollY > oldScrollY) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(5f);
            }
        }
        if (scrollY < oldScrollY && scrollY == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(0f);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_SCHOOL:
                    selectedSchool = (School) data.getSerializableExtra("school");
                    text_school.setText(selectedSchool.getNama());
                    schoolId = selectedSchool.getId();
                    break;
                case REQUEST_BRANCH:
                    selectedBranch = (Branch) data.getSerializableExtra("branch");
                    text_branch.setText(selectedBranch.getNama());
                    branchId = selectedBranch.getId();
                    break;
                case REQUEST_CLASS:
                    selectedClass = (Class) data.getSerializableExtra("class");
                    text_class.setText(selectedClass.getKelas());
                    classId = selectedClass.getId();
                    break;
            }
        }
    }
}