package com.pgedunesia.edunesia.utils;

public interface IOnBackPressed {

    boolean onBackPressed();

}
