package com.pgedunesia.edunesia.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.RequestBody;

public class CommonUtils {

    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static CircularProgressDrawable progressDrawable(Context context) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(50f);
        circularProgressDrawable.start();

        return circularProgressDrawable;
    }

    @NonNull
    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    public static void load_image(Context context, ImageView imageView, String imageUrl){
        PrefManager pref = PrefManager.getInstance(context);
        if (pref != null){
            Picasso.get()
                    .load(imageUrl)
                    .fit()
                    .centerCrop()
                    .placeholder(CommonUtils.progressDrawable(context))
                    .into(imageView);
        }else{
            Log.e("STATUS : ", "Tidak Ada");
            Picasso.get()
                    .load(R.drawable.ic_user)
                    .fit()
                    .centerCrop()
                    .placeholder(CommonUtils.progressDrawable(context))
                    .into(imageView);
        }
    }

    public static String convertFormatDate(String formatPattern) {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat(formatPattern);
        String currentDate = df.format(c);
        Log.d("current_date", currentDate);

        return currentDate;
    }

    public static double getScreenDimension(Context context){
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int dens = dm.densityDpi;
        double wi = (double)width / (double)dens;
        double hi = (double)height / (double)dens;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x+y);

        return screenInches;
    }

    public static long checkDate() {
        Calendar oldCalendar = Calendar.getInstance();
        Calendar newCalendar = Calendar.getInstance();
        oldCalendar.set(2019, 9, 25);
        newCalendar.set(2019, 9, 30);

        long oldDate = oldCalendar.getTimeInMillis();
        long nowDate = newCalendar.getTimeInMillis();

        long time = nowDate - oldDate;

        long selisih = time / 24 / 60 / 60 / 1000;

        return selisih;
    }
}
