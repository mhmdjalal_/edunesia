package com.pgedunesia.edunesia.base;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.pgedunesia.edunesia.R;

public class BaseActivity extends AppCompatActivity {

    public static String name = "name";
    public static String school = "school";
    public static String branch = "branch";
    public static String kelas = "class";
    public static String phone = "phone";
    public static String address = "address";
    public static String image = "image";

    ProgressDialog progressDialog;

    public void setUpActionBar(String title)
    {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void setHideActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    public void showLoading(String message)
    {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissLoading()
    {
        progressDialog.dismiss();
    }

    public void setupToolbar(Toolbar toolbar, @Nullable Drawable drawable) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (drawable != null) {
            getSupportActionBar().setHomeAsUpIndicator(drawable);
        }
    }
}
