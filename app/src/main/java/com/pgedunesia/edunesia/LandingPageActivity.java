package com.pgedunesia.edunesia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.parent.ui.login.ParentLoginActivity;
import com.pgedunesia.edunesia.parent.ui.main.ParentActivity;
import com.pgedunesia.edunesia.student.ui.home.HomeActivity;
import com.pgedunesia.edunesia.student.ui.login.LoginActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LandingPageActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        ButterKnife.bind(this);

        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        Calendar currentCal = Calendar.getInstance();
        String currentdate = dateFormat.format(currentCal.getTime());
        currentCal.add(Calendar.MONTH, 3);
        String toDate = dateFormat.format(currentCal.getTime());
        Log.e("Current Date", currentdate);
        Log.e("To Date", toDate);

        if (PrefManager.getInstance(this).getIsLogin()) {
            if (PrefManager.getInstance(this).getIsSiswa()) {
                startActivity(new Intent(this, HomeActivity.class));
            } else {
                startActivity(new Intent(this, ParentActivity.class));
            }
        }
    }

    @OnClick(R.id.card_parent)
    void gotoParent() {
        startActivity(new Intent(this, ParentLoginActivity.class));
    }

    @OnClick(R.id.card_student)
    void gotoStudent() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    static SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static long betweenDates(String firstDate, String secondDate){
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/London"));

        long daysDiff=0;
        long timeDiff=0;
        try {
            Date startDateObj = sdFormat.parse(firstDate);
            Date endDateObj = sdFormat.parse(secondDate);
            timeDiff = endDateObj.getTime() - startDateObj.getTime();
            daysDiff = timeDiff/(1000*60*60*24);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return daysDiff;
    }
}
