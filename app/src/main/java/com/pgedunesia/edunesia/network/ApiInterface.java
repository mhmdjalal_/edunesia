package com.pgedunesia.edunesia.network;

import com.pgedunesia.edunesia.data.response.ListQuizResponse;
import com.pgedunesia.edunesia.data.response.LoginOrangtuaResponse;
import com.pgedunesia.edunesia.data.response.StoreNilaiResponse;
import com.pgedunesia.edunesia.data.response.NilaiQuizResponse;
import com.pgedunesia.edunesia.data.response.UpdateParentResponse;
import com.pgedunesia.edunesia.data.response.ActivationResponse;
import com.pgedunesia.edunesia.data.response.BaseResponse;
import com.pgedunesia.edunesia.data.response.ParentResponse;
import com.pgedunesia.edunesia.data.response.ReportNilaiResponse;
import com.pgedunesia.edunesia.data.response.SearchSiswaResponse;
import com.pgedunesia.edunesia.data.response.BranchResponse;
import com.pgedunesia.edunesia.data.response.ClassResponse;
import com.pgedunesia.edunesia.data.response.LoginSiswaResponse;
import com.pgedunesia.edunesia.data.response.MapelResponse;
import com.pgedunesia.edunesia.data.response.RegisterResponse;
import com.pgedunesia.edunesia.data.response.SchoolResponse;
import com.pgedunesia.edunesia.data.response.StudentResponse;
import com.pgedunesia.edunesia.data.response.SoalResponse;
import com.pgedunesia.edunesia.data.response.UjianIdResponse;
import com.pgedunesia.edunesia.data.response.UjianResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface ApiInterface {

    @POST("login_siswa")
    @FormUrlEncoded
    Call<LoginSiswaResponse> loginSiswa(@Field("email") String email,
                                        @Field("password") String Password);

    @POST("login_orangtua")
    @FormUrlEncoded
    Call<LoginOrangtuaResponse> loginOrangtua(@Field("username") String username,
                                              @Field("password") String password);

    @GET("siswa/{id_siswa}")
    Call<RegisterResponse> showSiswa(@Path("id_siswa") int id_siswa);

    // Master Data

    @GET("cabangs")
    Call<BranchResponse> getAllCabang();

    @GET("sekolahs")
    Call<SchoolResponse> getAllSekolah();

    @GET("kelases")
    Call<ClassResponse> getAllKelas();

    @GET("mapels")
    Call<MapelResponse> getMapel();

    @GET("siswas")
    Call<StudentResponse> getAllSiswa();

    @GET("ujians/{id_siswa}/{mapel_id}")
    Call<UjianResponse> getUjian(@Path("id_siswa") int id_siswa, @Path("mapel_id") int mapel_id);

    @GET("soals/{id}/ujian")
    Call<SoalResponse> getSoal(@Path("id") int id);

    @GET("ujian/{id}")
    Call<UjianIdResponse> getUjianById(@Path("id") int id);

    @POST("register")
    @Multipart
    Call<RegisterResponse> registerSiswa(@Part MultipartBody.Part foto,
                                         @PartMap Map<String, RequestBody> body);

    @POST("update_siswa")
    @FormUrlEncoded
    Call<BaseResponse> updateProfilSiswa(@Field("id") int id,
                                         @Field("cabang_id") int cabang_id,
                                         @Field("sekolah_id") int sekolah_id,
                                         @Field("kelas_id") int kelas_id,
                                         @Field("nama") String nama,
                                         @Field("alamat") String alamat,
                                         @Field("no_telepon") String no_telepon);

    @POST("update_gambar")
    @Multipart
    Call<BaseResponse> updateGambar(@Part("id_siswa") int id_siswa,
                                     @Part MultipartBody.Part foto);

    @POST("check_aktivasi")
    @FormUrlEncoded
    Call<ActivationResponse> checkAktivasi(@Field("siswa_id") int siswa_id);

    @GET("orangtua/{parent_id}")
    Call<ParentResponse> getOrtuSiswa(@Path("parent_id") int parent_id);

    @GET("nilai/{siswa_id}")
    Call<ReportNilaiResponse> getNilaiSiswa(@Path("siswa_id") int siswa_id);

    @POST("search_siswa")
    @FormUrlEncoded
    Call<SearchSiswaResponse> searchSiswa(@Field("nama") String nama);

    @POST("update_orangtua")
    @FormUrlEncoded
    Call<UpdateParentResponse> updateOrangtua(@Field("id_orangtua") int id_orangtua,
                                              @Field("nama") String nama,
                                              @Field("no_telepon") String no_telepon);

    @GET("quizs/{siswa_id}")
    Call<ListQuizResponse> getListQuiz(@Path("siswa_id") int siswa_id);

    @GET("quizs/{siswa_id}/nilai")
    Call<NilaiQuizResponse> getNilaiQuiz(@Path("siswa_id") int siswa_id);

    @POST("store_quiz")
    @Multipart
    Call<BaseResponse> storeJawabanQuizPopup(@PartMap Map<String, RequestBody> body,
                                             @Part MultipartBody.Part gambar);


    @POST("store_jawaban")
    @FormUrlEncoded
    Call<StoreNilaiResponse> storeNilaiSiswa(@Field("siswa_id") int siswa_id,
                                             @Field("ujian_id") int ujian_id,
                                             @Field("nilai") int nilai);
}
