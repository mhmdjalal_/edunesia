package com.pgedunesia.edunesia.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.pgedunesia.edunesia.config.AppConfig.BASE_URL;
import static com.pgedunesia.edunesia.config.AppConfig.CONNECT_TIME;
import static com.pgedunesia.edunesia.config.AppConfig.READ_TIME;

public class ApiClient {
    private Retrofit retrofit;

    public Retrofit getRetrofit(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIME, TimeUnit.SECONDS)
                .readTimeout(READ_TIME, TimeUnit.SECONDS)
                .build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
}
