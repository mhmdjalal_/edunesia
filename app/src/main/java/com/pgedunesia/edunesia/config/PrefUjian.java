package com.pgedunesia.edunesia.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUjian {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static final int PRIVATE_MODE = 0;
    private static String PREF_NAME = "PrefUjian";
    Context context;

    public static PrefUjian getInstance(Context mContext) {
        return new PrefUjian(mContext);
    }

    public PrefUjian(Context mContext) {
        this.context = mContext;
        preferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getSoalJawaban(String key) {
        return preferences.getString(key, "");
    }

    public String setUserJawaban(int key, String soalJawaban) {
        editor.putString(String.valueOf(key), soalJawaban);
        editor.commit();
        return soalJawaban;
    }

    public void setUserJawabanNull(int key) {
        editor.putString(String.valueOf(key), null);
        editor.commit();
    }

    public String getJawabanBenar(String key) {
        return preferences.getString(key, "");
    }

    public String setJawabanBenar(String key, String soalJawaban) {
        editor.putString(String.valueOf(key), soalJawaban);
        editor.commit();
        return soalJawaban;
    }

    public void setUserJawabanBenarNull(String key) {
        editor.putString(String.valueOf(key), "");
        editor.commit();
    }

    public String getJawabanSalah(String key) {
        return preferences.getString(key, "");
    }

    public String setJawabanSalah(String key, String soalJawaban) {
        editor.putString(String.valueOf(key), soalJawaban);
        editor.commit();
        return soalJawaban;
    }

    public void setUserJawabanSalahNull(String key) {
        editor.putString(String.valueOf(key), "");
        editor.commit();
    }

    public String getSoalUdahDiisi() {
        return preferences.getString("ISI", "");
    }

    public String setSoalSudahDiisi(int jumlah) {
        editor.putString("ISI", String.valueOf(jumlah));
        editor.commit();
        return String.valueOf(jumlah);
    }

    public String getJumlahSoal() {
        return preferences.getString("JUMLAH_SOAL", "");
    }

    public String setJumlahSoal(int jumlah_soal) {
        editor.putString("JUMLAH_SOAL", String.valueOf(jumlah_soal));
        editor.commit();
        return String.valueOf(jumlah_soal);
    }

    public void setJumlahSoalNull() {
        editor.putString("JUMLAH_SOAL", null);
        editor.commit();
    }
}
