package com.pgedunesia.edunesia.config;

import android.content.Context;
import android.content.SharedPreferences;

import com.pgedunesia.edunesia.data.model.Parent;
import com.pgedunesia.edunesia.data.model.Student;

public class PrefManager {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static final int PRIVATE_MODE = 0;
    private static String PREF_NAME = "PrefManager";

    private static String IS_LOGIN = "isLogin";
    private static String IS_FIRST_TIME = "isFirstTime";
    private static String IS_SISWA = "isSiswa";

    private static String ID_ORTU = "idOrtu";
    private static String NAMA_ORTU = "namaOrtu";
    private static String HP_ORTU = "nohpOrtu";
    private static String USERNAME_ORTU = "usernameOrtu";
    private static String PASSWORD_ORTU = "passwordOrtu";

    private static String ALAMAT = "alamatSiswa";
    private static String CABANG = "cabangSiswa";
    private static String CABANG_ID = "cabangIdSiswa";
    private static String EMAIL = "emailSiswa";
    private static String FOTO = "fotoSiswa";
    private static String ID_SISWA = "idSiswa";
    private static String KELAS = "kelasSiswa";
    private static String KELAS_ID = "kelasIdSiswa";
    private static String NAMA = "namaSiswa";
    private static String NO_TELEPON = "noTeleponSiswa";
    private static String ORANGTUA = "orangtuaSiswa";
    private static String ORANGTUA_ID = "orangtuaIdSiswa";
    private static String SEKOLAH = "sekolahSiswa";
    private static String SEKOLAH_ID = "sekolahIdSiswa";

    public static PrefManager getInstance(Context mContext) {
        return new PrefManager(mContext);
    }

    public PrefManager(Context mContext) {
        preferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public boolean getIsLogin() {
        return preferences.getBoolean(IS_LOGIN, false);
    }

    public void setIsLogin(boolean isLogin) {
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.apply();
    }

    public boolean getIsFirstTime() {
        return preferences.getBoolean(IS_FIRST_TIME, true);
    }

    public void setIsFirstTime(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME, isFirstTime);
        editor.apply();
    }

    public boolean getIsSiswa() {
        return preferences.getBoolean(IS_SISWA, false);
    }

    public void setIsSiswa(boolean isSiswa) {
        editor.putBoolean(IS_SISWA, isSiswa);
        editor.apply();
    }

    public Parent getOrangtua() {
        return new Parent(preferences.getInt(ID_ORTU, 0),
                preferences.getString(NAMA_ORTU, ""),
                preferences.getString(HP_ORTU, ""),
                preferences.getString(USERNAME_ORTU, ""),
                preferences.getString(PASSWORD_ORTU, ""));
    }

    public void setOrangtua(Parent parent) {
        editor.putInt(ID_ORTU, parent.getId());
        editor.putString(NAMA_ORTU, parent.getNama());
        editor.putString(HP_ORTU, parent.getNoTelepon());
        editor.putString(USERNAME_ORTU, parent.getUsername());
        editor.putString(PASSWORD_ORTU, parent.getPassword());
        editor.apply();
    }

    public String getUserImage() {
        return preferences.getString(FOTO, "");
    }

    public String setUserImage(String userImage) {
        editor.putString(FOTO, userImage);
        editor.commit();
        return userImage;
    }

    public Student getSiswa() {
        return new Student(
                preferences.getString(ALAMAT, ""),
                preferences.getString(CABANG, ""),
                preferences.getInt(CABANG_ID, 0),
                preferences.getString(EMAIL, ""),
                preferences.getString(FOTO, ""),
                preferences.getInt(ID_SISWA, 0),
                preferences.getString(KELAS, ""),
                preferences.getInt(KELAS_ID, 0),
                preferences.getString(NAMA, ""),
                preferences.getString(NO_TELEPON, ""),
                preferences.getString(ORANGTUA, ""),
                preferences.getInt(ORANGTUA_ID, 0),
                preferences.getString(SEKOLAH, ""),
                preferences.getInt(SEKOLAH_ID, 0)
        );
    }

    public void setSiswa(Student student) {
        editor.putInt(ID_SISWA, student.getId());
        editor.putInt(CABANG_ID, student.getCabangId());
        editor.putInt(KELAS_ID, student.getKelasId());
        editor.putInt(ORANGTUA_ID, student.getOrangTuaId());
        editor.putInt(SEKOLAH_ID, student.getSekolahId());

        editor.putString(ALAMAT, student.getAlamat());
        editor.putString(CABANG, student.getCabang());
        editor.putString(EMAIL, student.getEmail());
        editor.putString(KELAS, student.getKelas());
        editor.putString(FOTO, student.getFoto());
        editor.putString(NAMA, student.getNama());
        editor.putString(NO_TELEPON, student.getNoTelepon());
        editor.putString(ORANGTUA, student.getOrangTua());
        editor.putString(SEKOLAH, student.getSekolah());
        editor.apply();
    }

}
