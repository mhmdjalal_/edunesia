package com.pgedunesia.edunesia.config;

public class AppConfig {

    public static int DELAY_LOADING = 3000;
    public static int DELAY_DIALOG = 5000;
    public static int DELAY_BACK = 2000;
    public static int DELAY_ALERT = 180000;
    public static String BASE_URL = "https://quiz.pgedunesia.com/api/";
    public static String IMAGE_SISWA_URL = "https://quiz.pgedunesia.com/images/siswa/";
    public static final int READ_TIME = 20;
    public static final int CONNECT_TIME = 10;

}
