
package com.pgedunesia.edunesia.data.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.School;

public class SchoolResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private List<School> data;

    public List<School> getData() {
        return data;
    }

    public void setData(List<School> data) {
        this.data = data;
    }

}
