
package com.pgedunesia.edunesia.data.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Student;

public class SearchSiswaResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private List<Student> data;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("status")
    private Boolean status;

    public List<Student> getData() {
        return data;
    }

    public void setData(List<Student> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
