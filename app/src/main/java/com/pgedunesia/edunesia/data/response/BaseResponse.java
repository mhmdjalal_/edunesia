package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @SerializedName("status")
    boolean status;
    @SerializedName("message")
    String message;

    public BaseResponse(boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
