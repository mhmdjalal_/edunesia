
package com.pgedunesia.edunesia.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NilaiUjian implements Serializable {

    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("kelas")
    private String kelas;

    @Expose
    @SerializedName("level")
    private String level;

    @Expose
    @SerializedName("mapel")
    private String mapel;

    @Expose
    @SerializedName("nilai")
    private int nilai;

    @Expose
    @SerializedName("siswa_id")
    private int siswaId;

    @Expose
    @SerializedName("ujian")
    private String ujian;

    @Expose
    @SerializedName("ujian_id")
    private int ujianId;

    @Expose
    @SerializedName("tanggal")
    private String tanggal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public int getNilai() {
        return nilai;
    }

    public void setNilai(int nilai) {
        this.nilai = nilai;
    }

    public int getSiswaId() {
        return siswaId;
    }

    public void setSiswaId(int siswaId) {
        this.siswaId = siswaId;
    }

    public String getUjian() {
        return ujian;
    }

    public void setUjian(String ujian) {
        this.ujian = ujian;
    }

    public int getUjianId() {
        return ujianId;
    }

    public void setUjianId(int ujianId) {
        this.ujianId = ujianId;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
