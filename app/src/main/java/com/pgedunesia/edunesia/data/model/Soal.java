package com.pgedunesia.edunesia.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Soal implements Serializable {

    @Expose
    @SerializedName("id")
    int id;

    @Expose
    @SerializedName("ujian_id")
    int ujian_id;

    @Expose
    @SerializedName("gambar")
    String gambar;

    @Expose
    @SerializedName("pertanyaan")
    String pertanyaan;

    @Expose
    @SerializedName("jawaban_a")
    String jawaban_a;

    @Expose
    @SerializedName("jawaban_b")
    String jawaban_b;

    @Expose
    @SerializedName("jawaban_c")
    String jawaban_c;

    @Expose
    @SerializedName("jawaban_d")
    String jawaban_d;

    @Expose
    @SerializedName("jawaban_benar")
    String jawaban_benar;

    @Expose
    @SerializedName("pembahasan")
    String pembahasan;

    @Expose
    @SerializedName("created_at")
    String created_at;

    @Expose
    @SerializedName("updated_at")
    String upated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUjian_id() {
        return ujian_id;
    }

    public void setUjian_id(int ujian_id) {
        this.ujian_id = ujian_id;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getJawaban_a() {
        return jawaban_a;
    }

    public void setJawaban_a(String jawaban_a) {
        this.jawaban_a = jawaban_a;
    }

    public String getJawaban_b() {
        return jawaban_b;
    }

    public void setJawaban_b(String jawaban_b) {
        this.jawaban_b = jawaban_b;
    }

    public String getJawaban_c() {
        return jawaban_c;
    }

    public void setJawaban_c(String jawaban_c) {
        this.jawaban_c = jawaban_c;
    }

    public String getJawaban_d() {
        return jawaban_d;
    }

    public void setJawaban_d(String jawaban_d) {
        this.jawaban_d = jawaban_d;
    }

    public String getJawaban_benar() {
        return jawaban_benar;
    }

    public void setJawaban_benar(String jawaban_benar) {
        this.jawaban_benar = jawaban_benar;
    }

    public String getPembahasan() {
        return pembahasan;
    }

    public void setPembahasan(String pembahasan) {
        this.pembahasan = pembahasan;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpated_at() {
        return upated_at;
    }

    public void setUpated_at(String upated_at) {
        this.upated_at = upated_at;
    }
}
