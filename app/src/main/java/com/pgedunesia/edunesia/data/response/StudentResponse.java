
package com.pgedunesia.edunesia.data.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Student;

public class StudentResponse {

    @SerializedName("status")
    String status;
    @SerializedName("message")
    String message;
    @SerializedName("data")
    private List<Student> data;

    public StudentResponse(String status, String message, List<Student> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public List<Student> getData() {
        return data;
    }

    public void setData(List<Student> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
