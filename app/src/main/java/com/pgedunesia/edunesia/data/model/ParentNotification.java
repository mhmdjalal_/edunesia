package com.pgedunesia.edunesia.data.model;

public class ParentNotification {

    String judul;
    String deskripsi;
    String tanggal;

    public ParentNotification() {
    }

    public ParentNotification(String judul, String deskripsi, String tanggal) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.tanggal = tanggal;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
