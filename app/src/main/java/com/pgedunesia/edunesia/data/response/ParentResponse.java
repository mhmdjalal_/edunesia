
package com.pgedunesia.edunesia.data.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Parent;
import com.pgedunesia.edunesia.data.model.Student;

public class ParentResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private Parent data;

    @Expose
    @SerializedName("siswas")
    private List<Student> students;

    @Expose
    @SerializedName("status")
    private Boolean status;

    public Parent getData() {
        return data;
    }

    public void setData(Parent data) {
        this.data = data;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
