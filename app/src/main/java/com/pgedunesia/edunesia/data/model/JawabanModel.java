package com.pgedunesia.edunesia.data.model;

public class JawabanModel {

    private int questions_id;
    private String jawaban;

    public JawabanModel(int questions_id, String jawaban) {
        this.questions_id = questions_id;
        this.jawaban = jawaban;
    }

    public int getQuestions_id() {
        return questions_id;
    }

    public void setQuestions_id(int questions_id) {
        this.questions_id = questions_id;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }
}