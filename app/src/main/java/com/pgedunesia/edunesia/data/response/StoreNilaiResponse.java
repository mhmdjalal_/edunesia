package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.SerializedName;

public class StoreNilaiResponse {

    @SerializedName("status")
    String status;
    @SerializedName("message")
    String message;
    @SerializedName("nilai")
    String nilai;

    public StoreNilaiResponse(String status, String message, String nilai) {
        this.status = status;
        this.message = message;
        this.nilai = nilai;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }
}
