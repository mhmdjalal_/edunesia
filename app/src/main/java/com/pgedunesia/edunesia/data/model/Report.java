package com.pgedunesia.edunesia.data.model;

public class Report {
    int nilai;
    String mapel;
    String tanggal;
    String type_ujian;
    String level;

    public Report() {
    }

    public Report(int nilai, String mapel, String tanggal, String type_ujian, String level) {
        this.nilai = nilai;
        this.mapel = mapel;
        this.tanggal = tanggal;
        this.type_ujian = type_ujian;
        this.level = level;
    }

    public int getNilai() {
        return nilai;
    }

    public void setNilai(int nilai) {
        this.nilai = nilai;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getType_ujian() {
        return type_ujian;
    }

    public void setType_ujian(String type_ujian) {
        this.type_ujian = type_ujian;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
