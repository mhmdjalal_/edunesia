package com.pgedunesia.edunesia.data.model;

public class Questions {

    String id, ujian_id, pertanyaan, jawaban_a, jawaban_b, jawaban_c, jawaban_d, jawaban_benar, pembahasan;

    public Questions(String id, String ujian_id, String pertanyaan, String jawaban_a, String jawaban_b, String jawaban_c, String jawaban_d, String jawaban_benar, String pembahasan) {
        this.id = id;
        this.ujian_id = ujian_id;
        this.pertanyaan = pertanyaan;
        this.jawaban_a = jawaban_a;
        this.jawaban_b = jawaban_b;
        this.jawaban_c = jawaban_c;
        this.jawaban_d = jawaban_d;
        this.jawaban_benar = jawaban_benar;
        this.pembahasan = pembahasan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUjian_id() {
        return ujian_id;
    }

    public void setUjian_id(String ujian_id) {
        this.ujian_id = ujian_id;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getJawaban_a() {
        return jawaban_a;
    }

    public void setJawaban_a(String jawaban_a) {
        this.jawaban_a = jawaban_a;
    }

    public String getJawaban_b() {
        return jawaban_b;
    }

    public void setJawaban_b(String jawaban_b) {
        this.jawaban_b = jawaban_b;
    }

    public String getJawaban_c() {
        return jawaban_c;
    }

    public void setJawaban_c(String jawaban_c) {
        this.jawaban_c = jawaban_c;
    }

    public String getJawaban_d() {
        return jawaban_d;
    }

    public void setJawaban_d(String jawaban_d) {
        this.jawaban_d = jawaban_d;
    }

    public String getJawaban_benar() {
        return jawaban_benar;
    }

    public void setJawaban_benar(String jawaban_benar) {
        this.jawaban_benar = jawaban_benar;
    }

    public String getPembahasan() {
        return pembahasan;
    }

    public void setPembahasan(String pembahasan) {
        this.pembahasan = pembahasan;
    }
}
