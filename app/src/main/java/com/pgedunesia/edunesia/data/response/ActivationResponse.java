package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.SerializedName;

public class ActivationResponse {

    @SerializedName("status")
    String status;

    @SerializedName("aktif")
    String aktif;

    public ActivationResponse(String status, String aktif) {
        this.status = status;
        this.aktif = aktif;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }
}
