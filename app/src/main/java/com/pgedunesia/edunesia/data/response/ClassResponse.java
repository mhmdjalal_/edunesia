
package com.pgedunesia.edunesia.data.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Class;

public class ClassResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private List<Class> data;

    public List<Class> getData() {
        return data;
    }

    public void setData(List<Class> data) {
        this.data = data;
    }

}
