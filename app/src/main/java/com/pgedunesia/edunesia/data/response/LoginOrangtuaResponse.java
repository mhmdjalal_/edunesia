
package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Parent;

import java.io.Serializable;

public class LoginOrangtuaResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private Parent orangTua;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("status")
    private Boolean status;

    public Parent getOrangTua() {
        return orangTua;
    }

    public void setOrangTua(Parent orangTua) {
        this.orangTua = orangTua;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
