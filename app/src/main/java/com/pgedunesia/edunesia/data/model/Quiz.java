package com.pgedunesia.edunesia.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Quiz implements Serializable {

	@SerializedName("kelas")
	private String kelas;

	@SerializedName("publish")
	private String publish;

	@SerializedName("id")
	private int id;

	@SerializedName("judul")
	private String judul;

	@SerializedName("gambar")
	private String gambar;

	@SerializedName("mapel")
	private String mapel;

	@SerializedName("status")
	private String status;

	@SerializedName("nilai")
	private int nilai;

	public void setKelas(String kelas){
		this.kelas = kelas;
	}

	public String getKelas(){
		return kelas;
	}

	public void setPublish(String publish){
		this.publish = publish;
	}

	public String getPublish(){
		return publish;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setJudul(String judul){
		this.judul = judul;
	}

	public String getJudul(){
		return judul;
	}

	public void setGambar(String gambar){
		this.gambar = gambar;
	}

	public String getGambar(){
		return gambar;
	}

	public void setMapel(String mapel){
		this.mapel = mapel;
	}

	public String getMapel(){
		return mapel;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public int getNilai() {
		return nilai;
	}

	public void setNilai(int nilai) {
		this.nilai = nilai;
	}
}