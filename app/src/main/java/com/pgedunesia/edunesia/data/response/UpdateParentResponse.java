package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Parent;

import java.io.Serializable;

public class UpdateParentResponse implements Serializable {

    @Expose
    @SerializedName("status")
    private boolean status;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("data")
    private Parent data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Parent getData() {
        return data;
    }

    public void setData(Parent data) {
        this.data = data;
    }
}
