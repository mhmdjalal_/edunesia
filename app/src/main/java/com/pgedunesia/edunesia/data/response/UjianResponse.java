package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Ujian;

import java.io.Serializable;
import java.util.List;

public class UjianResponse implements Serializable {

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("data")
    private List<Ujian> data;

    public List<Ujian> getData() {
        return data;
    }

    public void setData(List<Ujian> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
