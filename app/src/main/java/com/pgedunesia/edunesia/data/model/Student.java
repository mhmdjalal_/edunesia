
package com.pgedunesia.edunesia.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Student implements Serializable {

    @Expose
    @SerializedName("alamat")
    private String alamat;

    @Expose
    @SerializedName("cabang")
    private String cabang;

    @Expose
    @SerializedName("cabang_id")
    private int cabangId;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("foto")
    private String foto;

    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("kelas")
    private String kelas;

    @Expose
    @SerializedName("kelas_id")
    private int kelasId;

    @Expose
    @SerializedName("nama")
    private String nama;

    @Expose
    @SerializedName("no_telepon")
    private String noTelepon;

    @Expose
    @SerializedName("orang_tua")
    private String orangTua;

    @Expose
    @SerializedName("orang_tua_id")
    private int orangTuaId;

    @Expose
    @SerializedName("sekolah")
    private String sekolah;

    @Expose
    @SerializedName("sekolah_id")
    private int sekolahId;

    public Student(String alamat, String cabang, int cabangId, String email, String foto, int id, String kelas, int kelasId, String nama, String noTelepon, String orangTua, int orangTuaId, String sekolah, int sekolahId) {
        this.alamat = alamat;
        this.cabang = cabang;
        this.cabangId = cabangId;
        this.email = email;
        this.foto = foto;
        this.id = id;
        this.kelas = kelas;
        this.kelasId = kelasId;
        this.nama = nama;
        this.noTelepon = noTelepon;
        this.orangTua = orangTua;
        this.orangTuaId = orangTuaId;
        this.sekolah = sekolah;
        this.sekolahId = sekolahId;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public int getCabangId() {
        return cabangId;
    }

    public void setCabangId(int cabangId) {
        this.cabangId = cabangId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public int getKelasId() {
        return kelasId;
    }

    public void setKelasId(int kelasId) {
        this.kelasId = kelasId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoTelepon() {
        return noTelepon;
    }

    public void setNoTelepon(String noTelepon) {
        this.noTelepon = noTelepon;
    }

    public String getOrangTua() {
        return orangTua;
    }

    public void setOrangTua(String orangTua) {
        this.orangTua = orangTua;
    }

    public int getOrangTuaId() {
        return orangTuaId;
    }

    public void setOrangTuaId(int orangTuaId) {
        this.orangTuaId = orangTuaId;
    }

    public String getSekolah() {
        return sekolah;
    }

    public void setSekolah(String sekolah) {
        this.sekolah = sekolah;
    }

    public int getSekolahId() {
        return sekolahId;
    }

    public void setSekolahId(int sekolahId) {
        this.sekolahId = sekolahId;
    }

}
