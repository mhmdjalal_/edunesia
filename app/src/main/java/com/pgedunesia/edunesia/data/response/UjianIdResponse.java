package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Ujian;

import java.io.Serializable;

public class UjianIdResponse implements Serializable {

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("data")
    private Ujian data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Ujian getData() {
        return data;
    }

    public void setData(Ujian data) {
        this.data = data;
    }
}
