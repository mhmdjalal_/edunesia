package com.pgedunesia.edunesia.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ujian  implements Serializable {

    @Expose
    @SerializedName("id")
    String id;

    @Expose
    @SerializedName("kelas_id")
    String kelas_id;

    @Expose
    @SerializedName("mapel_id")
    String mapel_id;

    @Expose
    @SerializedName("mapel")
    String mapel;

    @Expose
    @SerializedName("nama")
    String nama;

    @Expose
    @SerializedName("level")
    String level;

    @Expose
    @SerializedName("waktu")
    String waktu;

    @Expose
    @SerializedName("publish")
    String publish;

    @Expose
    @SerializedName("status")
    String status;

    @Expose
    @SerializedName("created_at")
    String created_at;

    @Expose
    @SerializedName("updated_at")
    String updated_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKelas_id() {
        return kelas_id;
    }

    public void setKelas_id(String kelas_id) {
        this.kelas_id = kelas_id;
    }

    public String getMapel_id() {
        return mapel_id;
    }

    public void setMapel_id(String mapel_id) {
        this.mapel_id = mapel_id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
