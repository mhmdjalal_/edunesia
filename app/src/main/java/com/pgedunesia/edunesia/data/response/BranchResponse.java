
package com.pgedunesia.edunesia.data.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Branch;

public class BranchResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private List<Branch> data;

    public List<Branch> getData() {
        return data;
    }

    public void setData(List<Branch> data) {
        this.data = data;
    }

}
