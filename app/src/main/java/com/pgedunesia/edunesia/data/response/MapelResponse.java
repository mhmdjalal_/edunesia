package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Mapel;

import java.util.List;

public class MapelResponse {

    @Expose
    @SerializedName("data")
    private List<Mapel> data;

    public List<Mapel> getData() {
        return data;
    }

    public void setData(List<Mapel> data) {
        this.data = data;
    }

}
