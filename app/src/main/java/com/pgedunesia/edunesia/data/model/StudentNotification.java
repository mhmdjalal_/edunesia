package com.pgedunesia.edunesia.data.model;

public class StudentNotification {

    String title, mapel, date;

    public StudentNotification(String title, String mapel, String date) {
        this.title = title;
        this.mapel = mapel;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
