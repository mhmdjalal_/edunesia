package com.pgedunesia.edunesia.data.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Quiz;

public class ListQuizResponse implements Serializable {

	@SerializedName("data")
	private List<Quiz> data;

	@SerializedName("status")
	private boolean status;

	public void setData(List<Quiz> data){
		this.data = data;
	}

	public List<Quiz> getData(){
		return data;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}
}