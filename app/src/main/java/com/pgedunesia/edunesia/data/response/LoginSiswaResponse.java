
package com.pgedunesia.edunesia.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.Student;

import java.io.Serializable;

public class LoginSiswaResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private Student student;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("status")
    private Boolean status;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
