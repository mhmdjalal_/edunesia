
package com.pgedunesia.edunesia.data.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pgedunesia.edunesia.data.model.NilaiUjian;

public class ReportNilaiResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private List<NilaiUjian> data;

    @Expose
    @SerializedName("status")
    private Boolean status;

    public List<NilaiUjian> getData() {
        return data;
    }

    public void setData(List<NilaiUjian> data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
