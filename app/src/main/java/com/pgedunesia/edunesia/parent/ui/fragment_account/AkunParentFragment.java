package com.pgedunesia.edunesia.parent.ui.fragment_account;


import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Parent;
import com.pgedunesia.edunesia.data.model.Student;
import com.pgedunesia.edunesia.parent.adapter.AdapterDataStudent;
import com.pgedunesia.edunesia.parent.ui.login.ParentLoginActivity;
import com.pgedunesia.edunesia.parent.ui.main.MainParentViewModel;
import com.pgedunesia.edunesia.parent.ui.updata_parent.EditOrangtuaActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AkunParentFragment extends Fragment {

    @BindView(R.id.button_logout)
    Button btn_logout;
    @BindView(R.id.text_username)
    TextView text_username;
    @BindView(R.id.text_no_telepon)
    TextView text_noTelepon;
    @BindView(R.id.image_edit)
    ImageView imageEdit;
    @BindView(R.id.frame)
    FrameLayout frameLayout;
    @BindView(R.id.recycler_student)
    RecyclerView recycler_student;
    @BindView(R.id.text_title_student)
    TextView text_title_student;

    private Parent parent;
    private PrefManager mPrefManager;

    MainParentViewModel mViewModel;

    AdapterDataStudent adapterDataStudent;

    public AkunParentFragment() {
        // Required empty public constructor
    }

    public static AkunParentFragment newInstance(Parent parent) {
        AkunParentFragment fragment = new AkunParentFragment();
        Bundle args = new Bundle();
        args.putSerializable("parent", parent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            parent = (Parent) getArguments().getSerializable("parent");
        }
        mPrefManager = PrefManager.getInstance(getContext());
        adapterDataStudent = new AdapterDataStudent(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_akun_parent, container, false);
        ButterKnife.bind(this, v);

        mViewModel = ViewModelProviders.of(this).get(MainParentViewModel.class);
        mViewModel.getSiswas().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(List<Student> students) {
                if (students != null) {
                    adapterDataStudent.updateData(students);
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recycler_student.setLayoutManager(layoutManager);
        recycler_student.setAdapter(adapterDataStudent);

        text_username.setText((mPrefManager.getOrangtua().getNama().equals("")) ? "-" : mPrefManager.getOrangtua().getNama());
        text_noTelepon.setText((mPrefManager.getOrangtua().getNoTelepon().equals("")) ? "-" : mPrefManager.getOrangtua().getNoTelepon());
        imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditOrangtuaActivity.class);
                intent.putExtra("parent", mPrefManager.getOrangtua());
                startActivity(intent);
            }
        });
        loadAnimation();
        return v;
    }

    void loadAnimation() {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
        frameLayout.startAnimation(animation);
        Animation recycler_animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_right);
        recycler_student.startAnimation(recycler_animation);
        text_title_student.startAnimation(recycler_animation);
    }

    @OnClick(R.id.button_logout)
    void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View v = getLayoutInflater().inflate(R.layout.dialog_konfirmasi_logout, null);
        TextView text_yes = v.findViewById(R.id.text_yes);
        TextView text_no = v.findViewById(R.id.text_no);
        builder.setView(v);

        AlertDialog alert = builder.create();
        text_yes.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), ParentLoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PrefManager.getInstance(getContext()).setIsLogin(false);
            startActivity(intent);
            getActivity().finish();
        });
        text_no.setOnClickListener(view -> alert.cancel());
        alert.show();
    }

}
