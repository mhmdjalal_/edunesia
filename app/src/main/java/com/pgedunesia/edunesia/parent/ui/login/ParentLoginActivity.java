package com.pgedunesia.edunesia.parent.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.response.LoginOrangtuaResponse;
import com.pgedunesia.edunesia.parent.ui.main.ParentActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParentLoginActivity extends AppCompatActivity {

    @BindView(R.id.edittext_username)
    EditText etUsername;
    @BindView(R.id.edittext_password)
    EditText etPassword;
    @BindView(R.id.bottom_section)
    LinearLayout bottom_section;
    @BindView(R.id.text_get_started)
    TextView text_get_started;
    @BindView(R.id.row_top)
    FrameLayout frameLayout;

    LoginOrtuViewModel mViewModel;

    PrefManager prefManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_login);
        ButterKnife.bind(this);
        responsiveScreen();

        prefManager = PrefManager.getInstance(this);
        progressDialog = new ProgressDialog(this);

        mViewModel = ViewModelProviders.of(this).get(LoginOrtuViewModel.class);
        mViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading != null) {
                    if (isLoading) {
                        progressDialog.setMessage("Loading...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }
        });
        mViewModel.getLoginResponse().observe(this, new Observer<LoginOrangtuaResponse>() {
            @Override
            public void onChanged(LoginOrangtuaResponse loginOrangtuaResponse) {
                if (loginOrangtuaResponse != null) {
                    if (loginOrangtuaResponse.getStatus()) {
                        prefManager.setOrangtua(loginOrangtuaResponse.getOrangTua());
                        prefManager.setIsLogin(true);
                        prefManager.setIsSiswa(false);
                        startActivity(new Intent(ParentLoginActivity.this, ParentActivity.class));
                        finish();
                    } else {
                        Snackbar.make(etPassword, loginOrangtuaResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        loadAnimation();
    }

    void loadAnimation() {
        Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        bottom_section.startAnimation(slideUp);
        Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        frameLayout.startAnimation(slideDown);
    }

    @OnClick(R.id.image_back)
    void onBack() {
        super.onBackPressed();
    }

    @OnClick(R.id.button_login)
    void login() {
        if (etUsername.getText().toString().trim().isEmpty()) {
            etUsername.requestFocus();
            etUsername.setError("Username kosong");
        } else if (etPassword.getText().toString().trim().isEmpty()) {
            etPassword.requestFocus();
            etPassword.setError("Password kosong");
        } else {
            mViewModel.loginOrangTua(etUsername.getText().toString().trim(), etPassword.getText().toString().trim());
        }
    }

    void responsiveScreen() {
        double screenSize = CommonUtils.getScreenDimension(this);
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
            frameLayout.setLayoutParams(params);
        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 380);
            frameLayout.setLayoutParams(params);
        }
    }
}
