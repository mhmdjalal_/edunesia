package com.pgedunesia.edunesia.parent.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Student;
import com.pgedunesia.edunesia.parent.ui.student_detail.DetailSiswaActivity;
import com.pgedunesia.edunesia.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterDataStudent extends RecyclerView.Adapter<AdapterDataStudent.DataStudentViewHolder> {

    private Context mContext;
    private List<Student> students = new ArrayList<>();

    public AdapterDataStudent(Context mContext) {
        this.mContext = mContext;
    }

    public void updateData(List<Student> students) {
        this.students = students;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DataStudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DataStudentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_student, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DataStudentViewHolder holder, int position) {
        holder.bindView(students.get(position));
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class DataStudentViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_kelas)
        TextView text_kelas;
        @BindView(R.id.text_sekolah)
        TextView text_sekolah;
        @BindView(R.id.text_phone)
        TextView text_phone;
        @BindView(R.id.text_alamat)
        TextView text_alamat;
        @BindView(R.id.text_cabang)
        TextView text_cabang;
        @BindView(R.id.text_nama)
        TextView text_nama;
        @BindView(R.id.image_student)
        CircleImageView image_student;
        @BindView(R.id.image_detail)
        ImageView image_detail;
        @BindView(R.id.row_data)
        LinearLayout container;

        public DataStudentViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(Student student) {
            text_nama.setText(student.getNama());
            text_kelas.setText(student.getKelas());
            text_sekolah.setText(student.getSekolah());
            text_phone.setText(student.getNoTelepon());
            text_alamat.setText(student.getAlamat());
            text_cabang.setText(student.getCabang());
            CommonUtils.load_image(mContext, image_student, student.getFoto());

            image_detail.setOnClickListener(view -> {
                Intent intent = new Intent(mContext, DetailSiswaActivity.class);
                intent.putExtra("student", student);
                mContext.startActivity(intent);
            });
            responsiveScreen(container);
        }
    }

    void responsiveScreen(View view) {
        double screenSize = CommonUtils.getScreenDimension(mContext);
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(370, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 10, 20, 20);
            view.setLayoutParams(params);
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(550, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 10, 20, 40);
            view.setLayoutParams(params);
        }
    }
}
