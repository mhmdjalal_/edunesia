package com.pgedunesia.edunesia.parent.ui.student_data_fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Student;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataSiswaFragment extends Fragment {
    private static final String SISWA = "student";

    @BindView(R.id.text_kelas)
    TextView text_kelas;
    @BindView(R.id.text_sekolah)
    TextView text_sekolah;
    @BindView(R.id.text_no_telepon)
    TextView text_no_telepon;
    @BindView(R.id.text_alamat)
    TextView text_alamat;
    @BindView(R.id.text_cabang)
    TextView text_cabang;
    @BindView(R.id.row_data)
    MaterialCardView cardView;

    private Student student;

    public DataSiswaFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DataSiswaFragment newInstance(Student student) {
        DataSiswaFragment fragment = new DataSiswaFragment();
        Bundle args = new Bundle();
        args.putSerializable(SISWA, student);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            student = (Student) getArguments().getSerializable(SISWA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_data_siswa, container, false);
        ButterKnife.bind(this, v);

        text_kelas.setText(student.getKelas());
        text_sekolah.setText(student.getSekolah());
        text_no_telepon.setText(student.getNoTelepon());
        text_alamat.setText(student.getAlamat());
        text_cabang.setText(student.getCabang());
        return v;
    }

//    @OnClick(R.id.button_claim_student)
//    void claimStudent() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//
//        View v = getLayoutInflater().inflate(R.layout.dialog_ortu_request, null);
//        TextView text_title = v.findViewById(R.id.text_title);
//        TextView text_message = v.findViewById(R.id.text_message);
//        TextView text_yes = v.findViewById(R.id.text_yes);
//
//        builder.setView(v);
//
//        AlertDialog alert = builder.create();
//        text_title.setText("Edu Quiz");
//        text_message.setText("Permintaan diproses, silakan hubungi anak Anda untuk menerima permintaan sebagai Orang Tua");
//        text_yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alert.cancel();
//            }
//        });
//
//        ProgressDialog progressDialog = new ProgressDialog(getContext());
//        progressDialog.setMessage("Loading..."); // Setting Message
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
//        progressDialog.show(); // Display Progress Dialog
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                progressDialog.dismiss();
//                alert.show();
//            }
//        }, 5000);
//    }
}
