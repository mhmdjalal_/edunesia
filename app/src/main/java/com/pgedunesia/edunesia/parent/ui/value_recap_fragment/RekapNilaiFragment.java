package com.pgedunesia.edunesia.parent.ui.value_recap_fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.parent.adapter.AdapterRekapNilai;
import com.pgedunesia.edunesia.data.model.NilaiUjian;
import com.pgedunesia.edunesia.data.model.Report;
import com.pgedunesia.edunesia.parent.ui.student_detail.DetailSiswaViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RekapNilaiFragment extends Fragment {

    @BindView(R.id.recycler_nilai)
    RecyclerView recycler_nilai;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.view_no_item_found)
    LinearLayout no_item_found;
    @BindView(R.id.container)
    RelativeLayout container;

    DetailSiswaViewModel mViewModel;
    AdapterRekapNilai mAdapterRekapNilai;
    List<Report> reports = new ArrayList<>();

    private static final String SISWA_ID = "siswa_id";

    private int siswaId = 0;

    public RekapNilaiFragment() {
        // Required empty public constructor
    }

    public static RekapNilaiFragment newInstance(int siswa_id) {
        RekapNilaiFragment fragment = new RekapNilaiFragment();
        Bundle args = new Bundle();
        args.putInt(SISWA_ID, siswa_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(DetailSiswaViewModel.class);
        mViewModel.getNilaiUjian().observe(this, new Observer<List<NilaiUjian>>() {
            @Override
            public void onChanged(List<NilaiUjian> nilais) {
                refreshLayout.setRefreshing(false);
                if (nilais != null) {
                    Log.d("ReportNilai", "size " + nilais.size());
                    if (nilais.size() > 0) {
                        mAdapterRekapNilai.updateData(nilais);
                    } else {
                        no_item_found.setVisibility(View.VISIBLE);
                        recycler_nilai.setVisibility(View.GONE);
                    }
                }
            }
        });

        if (getArguments() != null) {
            siswaId = getArguments().getInt(SISWA_ID);
            mViewModel.loadNilaiUjian(siswaId);
        }

        mAdapterRekapNilai = new AdapterRekapNilai(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_rekap_nilai, container, false);
        ButterKnife.bind(this, v);

        recycler_nilai.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_nilai.setAdapter(mAdapterRekapNilai);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mViewModel.loadNilaiUjian(siswaId);
            }
        });

        return v;
    }
}
