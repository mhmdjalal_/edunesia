package com.pgedunesia.edunesia.parent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.ParentNotification;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterNotifikasi extends RecyclerView.Adapter<AdapterNotifikasi.NotifikasiViewHolder> {

    private Context mContext;
    private List<ParentNotification> parentNotifications;
    OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(ParentNotification parentNotification);
    }

    public AdapterNotifikasi(Context mContext) {
        this.mContext = mContext;
    }

    public AdapterNotifikasi(Context mContext, List<ParentNotification> parentNotifications, OnItemClickListener listener) {
        this.mContext = mContext;
        this.parentNotifications = parentNotifications;
        this.listener = listener;
    }

    @NonNull
    @Override
    public NotifikasiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotifikasiViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notifikasi, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotifikasiViewHolder holder, int position) {
        holder.bindItem(parentNotifications.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return parentNotifications.size();
    }

    public class NotifikasiViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_judul)
        TextView text_judul;
        @BindView(R.id.text_desc)
        TextView text_desc;
        @BindView(R.id.text_tanggal)
        TextView text_tanggal;

        public NotifikasiViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindItem(ParentNotification parentNotification, OnItemClickListener listener) {
            text_judul.setText(parentNotification.getJudul());
            text_desc.setText(parentNotification.getDeskripsi());
            text_tanggal.setText(parentNotification.getTanggal());

            itemView.setOnClickListener(view -> {
               listener.onItemClick(parentNotification);
            });
        }
    }

    public void updateData(List<ParentNotification> parentNotifications) {
        this.parentNotifications = parentNotifications;
        notifyDataSetChanged();
    }
}
