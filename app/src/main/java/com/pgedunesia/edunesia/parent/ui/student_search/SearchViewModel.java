package com.pgedunesia.edunesia.parent.ui.student_search;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.data.model.Student;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.SearchSiswaResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchViewModel extends AndroidViewModel {
    private MutableLiveData<List<Student>> mSiswas = new MutableLiveData<>();
    private MutableLiveData<SearchSiswaResponse> mSearchResponse = new MutableLiveData<>();

    ApiInterface apiInterface;

    public SearchViewModel(@NonNull Application application) {
        super(application);

        mSiswas.setValue(new ArrayList<>());
        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
    }

    public LiveData<List<Student>> getSiswa() {
        return mSiswas;
    }

    public LiveData<SearchSiswaResponse> getSearchSiswa() {
        return mSearchResponse;
    }

    public void searchSiswa(String nama) {
        apiInterface.searchSiswa(nama)
                .enqueue(new Callback<SearchSiswaResponse>() {
                    @Override
                    public void onResponse(Call<SearchSiswaResponse> call, Response<SearchSiswaResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()) {
                                mSiswas.postValue(response.body().getData());
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<SearchSiswaResponse> call, Throwable t) {

                    }
                });
    }
}
