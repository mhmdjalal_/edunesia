package com.pgedunesia.edunesia.parent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Quiz;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterNilaiQuiz extends RecyclerView.Adapter<AdapterNilaiQuiz.RekapNilaiViewHolder> {

    private Context mContext;
    private List<Quiz> nilais = new ArrayList<>();

    public AdapterNilaiQuiz(Context mContext) {
        this.mContext = mContext;
    }

    public AdapterNilaiQuiz(Context mContext, List<Quiz> nilais) {
        this.mContext = mContext;
        this.nilais = nilais;
    }

    @NonNull
    @Override
    public RekapNilaiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RekapNilaiViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nilai_quiz, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RekapNilaiViewHolder holder, int position) {
        Quiz nilai = nilais.get(position);

        switch (nilai.getStatus().toLowerCase()) {
            case "selesai":
                holder.bg_type.setBackground(mContext.getResources().getDrawable(R.drawable.gradient_cover_radius));
                holder.text_skor.setText(String.valueOf(nilai.getNilai()));
                holder.text_mapel.setText(nilai.getMapel());
                holder.text_tanggal.setText(nilai.getPublish());
                holder.text_status.setText(nilai.getJudul());
                holder.view_trophy.setVisibility(View.VISIBLE);
                holder.view_trophy.removeAllViews();

                if (nilai.getNilai() < 40) {
                    ImageView imageView = new ImageView(mContext);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
                    params.setMargins(10, 10, 10, 10);
                    imageView.setLayoutParams(params);
                    imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_trophy));
                    holder.view_trophy.addView(imageView);
                } else if (nilai.getNilai() > 40 && nilai.getNilai() < 90) {
                    for (int i = 0; i < 2; i++) {
                        ImageView imageView = new ImageView(mContext);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
                        params.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(params);
                        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_trophy));
                        holder.view_trophy.addView(imageView);
                    }
                } else if (nilai.getNilai() >= 90) {
                    for (int i = 0; i < 3; i++) {
                        ImageView imageView = new ImageView(mContext);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
                        params.setMargins(10, 10, 10, 10);
                        imageView.setLayoutParams(params);
                        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_trophy));
                        holder.view_trophy.addView(imageView);
                    }
                }
                return;
            case "sedang dikoreksi":
                holder.text_skor.setTextSize(22f);
                holder.text_skor.setText(nilai.getJudul());
                holder.text_mapel.setText(nilai.getMapel());
                holder.text_tanggal.setText(nilai.getPublish());
                holder.view_trophy.setVisibility(View.GONE);
                holder.bg_type.setBackground(mContext.getResources().getDrawable(R.drawable.bg_gradient_orange));
                holder.text_status.setText(nilai.getStatus());
                return;
        }
    }

    @Override
    public int getItemCount() {
        return nilais.size();
    }

    public void updateData(List<Quiz> nilais) {
        this.nilais = nilais;
        notifyDataSetChanged();
    }

    public void clearData() {
        nilais.clear();
        notifyDataSetChanged();
    }

    public class RekapNilaiViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.text_skor)
        TextView text_skor;
        @BindView(R.id.text_mapel)
        TextView text_mapel;
        @BindView(R.id.text_type_ujian)
        TextView text_status;
        @BindView(R.id.text_tanggal)
        TextView text_tanggal;
        @BindView(R.id.view_trophy)
        LinearLayout view_trophy;
        @BindView(R.id.bg_type)
        FrameLayout bg_type;

        public RekapNilaiViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
