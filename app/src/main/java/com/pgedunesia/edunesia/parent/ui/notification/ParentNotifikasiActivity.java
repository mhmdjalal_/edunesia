package com.pgedunesia.edunesia.parent.ui.notification;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.parent.adapter.AdapterNotifikasi;
import com.pgedunesia.edunesia.data.model.ParentNotification;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentNotifikasiActivity extends BaseActivity {

    @BindView(R.id.recycler_notifikasi)
    RecyclerView recycler_notifikasi;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private AdapterNotifikasi adapterNotifikasi;
    private List<ParentNotification> parentNotifications = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_notifikasi);
        ButterKnife.bind(this);
        setupToolbar();

        for (int i = 0; i < 10; i++) {
            parentNotifications.add(new ParentNotification("Judul "+(i+1), getResources().getString(R.string.dummy_text), "20-Aug-2019"));
        }

        adapterNotifikasi = new AdapterNotifikasi(this, parentNotifications, parentNotification -> {
            Toast.makeText(this, parentNotification.getJudul(), Toast.LENGTH_SHORT).show();
        });

        initAdapter();
    }

    void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black);
    }

    void initAdapter() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recycler_notifikasi.setHasFixedSize(false);
        recycler_notifikasi.addItemDecoration(dividerItemDecoration);
        recycler_notifikasi.setLayoutManager(new LinearLayoutManager(this));
        recycler_notifikasi.setNestedScrollingEnabled(false);
        recycler_notifikasi.setAdapter(adapterNotifikasi);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
