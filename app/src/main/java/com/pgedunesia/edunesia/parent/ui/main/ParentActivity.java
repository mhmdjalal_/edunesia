package com.pgedunesia.edunesia.parent.ui.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.parent.ui.fragment_account.AkunParentFragment;
import com.pgedunesia.edunesia.parent.ui.fragment_home.HomeParentFragment;
import com.pgedunesia.edunesia.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentActivity extends AppCompatActivity {

    @BindView(R.id.nav_view)
    BottomNavigationView bottomNavigationView;

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Menu menu = bottomNavigationView.getMenu();
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.ic_home_unselected);
            menu.findItem(R.id.navigation_akun).setIcon(R.drawable.ic_profile_unselected);

            switch (menuItem.getItemId()) {
                case R.id.navigation_home:
                    menu.findItem(R.id.navigation_home).setIcon(R.drawable.ic_home_selected);
                    loadFragment(HomeParentFragment.newInstance(PrefManager.getInstance(ParentActivity.this).getOrangtua()));
                    return true;
                case R.id.navigation_akun:
                    menu.findItem(R.id.navigation_akun).setIcon(R.drawable.ic_profile_selected);
                    loadFragment(AkunParentFragment.newInstance(PrefManager.getInstance(ParentActivity.this).getOrangtua()));
                    return true;
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
        ButterKnife.bind(this);
        responsiveScreen();

        loadFragment(HomeParentFragment.newInstance(PrefManager.getInstance(this).getOrangtua()));
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
    }

    void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.frame_container, fragment)
                .commit();
    }

    void responsiveScreen() {
        double screenSize = CommonUtils.getScreenDimension(this);
        Log.d("SCREEN_SIZE", String.valueOf(screenSize));
        if (screenSize < 4) {
            bottomNavigationView.setPadding(0,0,0,0);
            bottomNavigationView.setItemIconSize(24);
        } else {
            bottomNavigationView.setPadding(0,12,0,12);
            bottomNavigationView.setItemIconSize(35);
        }
    }
}
