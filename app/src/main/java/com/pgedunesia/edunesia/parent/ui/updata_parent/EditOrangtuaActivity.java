package com.pgedunesia.edunesia.parent.ui.updata_parent;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Parent;
import com.pgedunesia.edunesia.data.response.UpdateParentResponse;
import com.pgedunesia.edunesia.parent.ui.main.ParentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditOrangtuaActivity extends BaseActivity {

    @BindView(R.id.edittext_nama)
    EditText etNama;
    @BindView(R.id.edittext_phone)
    EditText etPhone;

    EditOrangtuaViewModel mViewModel;
    PrefManager prefManager;
    Parent parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_orangtua);
        ButterKnife.bind(this);

        prefManager = PrefManager.getInstance(this);
        mViewModel = ViewModelProviders.of(this).get(EditOrangtuaViewModel.class);
        mViewModel.getUpdateResponse().observe(this, new Observer<UpdateParentResponse>() {
            @Override
            public void onChanged(UpdateParentResponse response) {
                if (response != null) {
                    if (response.isStatus()) {
                        prefManager.setOrangtua(response.getData());
                        startActivity(new Intent(EditOrangtuaActivity.this, ParentActivity.class));
                        finish();
                    } else {
                        Toast.makeText(EditOrangtuaActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        etNama.setText(prefManager.getOrangtua().getNama());
        etPhone.setText(prefManager.getOrangtua().getNoTelepon());
        setUpActionBar("Edit Profile");
    }

    @OnClick(R.id.button_ubah)
    void onClickUpdate() {
        mViewModel.updateOrangtua(etNama.getText().toString().trim(), etPhone.getText().toString().trim());
    }

    @Override
    public void setUpActionBar(String title) {
        super.setUpActionBar(title);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
