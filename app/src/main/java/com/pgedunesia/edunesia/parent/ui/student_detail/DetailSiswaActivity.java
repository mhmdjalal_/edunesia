package com.pgedunesia.edunesia.parent.ui.student_detail;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.base.BaseActivity;
import com.pgedunesia.edunesia.data.model.Student;
import com.pgedunesia.edunesia.parent.adapter.SiswaPagerAdapter;
import com.pgedunesia.edunesia.student.fragment.ExamValueFragment;
import com.pgedunesia.edunesia.student.fragment.QuizValueFragment;
import com.pgedunesia.edunesia.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailSiswaActivity extends BaseActivity {

    @BindView(R.id.image_siswa)
    CircleImageView image_siswa;
    @BindView(R.id.text_nama_siswa)
    TextView text_nama_siswa;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tablayout)
    TabLayout tabLayout;

    SiswaPagerAdapter mSiswaPagerAdapter;

    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_siswa);
        ButterKnife.bind(this);

        student = (Student) getIntent().getSerializableExtra("student");
        Log.d("SISWA_ID", "detail " + String.valueOf(student.getId()));

        mSiswaPagerAdapter = new SiswaPagerAdapter(getSupportFragmentManager());
        mSiswaPagerAdapter.addFragment(ExamValueFragment.newInstance(student.getId()), "Nilai Ujian");
        mSiswaPagerAdapter.addFragment(QuizValueFragment.newInstance(student.getId()), "Nilai Quiz");
        viewPager.setAdapter(mSiswaPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        text_nama_siswa.setText(student.getNama());
        CommonUtils.load_image(this, image_siswa, student.getFoto());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.image_back)
    void onClickBackPressed() {
        super.onBackPressed();
    }
}
