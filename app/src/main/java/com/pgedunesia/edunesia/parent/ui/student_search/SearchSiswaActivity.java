package com.pgedunesia.edunesia.parent.ui.student_search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Student;
import com.pgedunesia.edunesia.parent.adapter.AdapterSiswa;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchSiswaActivity extends AppCompatActivity {

    @BindView(R.id.recycler_student)
    RecyclerView recycler_student;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.view_no_item_found)
    LinearLayout no_item_found;
    @BindView(R.id.edittext_search)
    EditText et_search;

    SearchViewModel mViewModel;

    AdapterSiswa mAdapterSiswa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_siswa);
        ButterKnife.bind(this);

        mViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        mViewModel.getSiswa().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(List<Student> students) {
                refreshLayout.setRefreshing(false);
                if (students != null) {
                    if (students.size() > 0) {
                        no_item_found.setVisibility(View.GONE);
                        recycler_student.setVisibility(View.VISIBLE);
                        mAdapterSiswa.updateData(students);
                    } else {
                        no_item_found.setVisibility(View.VISIBLE);
                        recycler_student.setVisibility(View.GONE);
                    }
                }
            }
        });

        mAdapterSiswa = new AdapterSiswa(this);
        recycler_student.setLayoutManager(new LinearLayoutManager(this));
        recycler_student.setAdapter(mAdapterSiswa);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mViewModel.searchSiswa(et_search.getText().toString());
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    refreshLayout.setRefreshing(true);
                    mViewModel.searchSiswa(charSequence.toString());
                } else {
                    mViewModel.getSiswa().getValue().clear();
                    no_item_found.setVisibility(View.VISIBLE);
                    recycler_student.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().trim().length() < 1) {
                    mViewModel.getSiswa().getValue().clear();
                    no_item_found.setVisibility(View.VISIBLE);
                    recycler_student.setVisibility(View.GONE);
                }
            }
        });
    }

    @OnClick(R.id.image_back)
    void onBack() {
        super.onBackPressed();
    }
}
