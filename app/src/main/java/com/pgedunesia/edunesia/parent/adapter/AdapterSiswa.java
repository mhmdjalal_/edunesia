package com.pgedunesia.edunesia.parent.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.data.model.Student;
import com.pgedunesia.edunesia.parent.ui.student_detail.DetailSiswaActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterSiswa extends RecyclerView.Adapter<AdapterSiswa.SiswaViewHolder> {

    Context mContext;
    List<Student> students = new ArrayList<>();

    public AdapterSiswa(Context mContext) {
        this.mContext = mContext;
    }

    public AdapterSiswa(Context mContext, List<Student> students) {
        this.mContext = mContext;
        this.students = students;
    }

    @NonNull
    @Override
    public SiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SiswaViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student_search, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SiswaViewHolder holder, int position) {
        Student student = students.get(position);

        holder.text_nama_siswa.setText(student.getNama());
        holder.text_kelas.setText(student.getKelas());
        holder.text_sekolah.setText(student.getSekolah());
        holder.text_cabang.setText(student.getCabang());

        Picasso.get()
                .load(student.getFoto())
                .error(R.drawable.ic_user)
                .into(holder.image_siswa);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailSiswaActivity.class);
                intent.putExtra("student", student);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public void updateData(List<Student> students) {
        this.students = students;
        notifyDataSetChanged();
    }

    public class SiswaViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.text_nama_siswa)
        TextView text_nama_siswa;
        @BindView(R.id.text_kelas)
        TextView text_kelas;
        @BindView(R.id.text_sekolah)
        TextView text_sekolah;
        @BindView(R.id.text_cabang)
        TextView text_cabang;
        @BindView(R.id.image_siswa)
        CircleImageView image_siswa;

        public SiswaViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
