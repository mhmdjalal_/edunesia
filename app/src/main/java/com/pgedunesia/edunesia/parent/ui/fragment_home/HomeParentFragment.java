package com.pgedunesia.edunesia.parent.ui.fragment_home;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.pgedunesia.edunesia.R;
import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Parent;
import com.pgedunesia.edunesia.parent.adapter.AdapterSiswa;
import com.pgedunesia.edunesia.parent.ui.main.MainParentViewModel;
import com.pgedunesia.edunesia.parent.ui.notification.ParentNotifikasiActivity;
import com.pgedunesia.edunesia.data.model.Student;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeParentFragment extends Fragment implements NestedScrollView.OnScrollChangeListener {
    private static final String ARG_PARAM1 = "param1";

    @BindView(R.id.recycler_student)
    RecyclerView recycler_student;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.nestedScroll_parent)
    NestedScrollView nestedScrollView;
    @BindView(R.id.image_notification)
    ImageView image_notifikasi;
    @BindView(R.id.text_nama)
    TextView text_nama;
    @BindView(R.id.text_desc)
    TextView text_desc;
    @BindView(R.id.progress_content)
    ProgressBar progress_content;

    AdapterSiswa mAdapterSiswa;
    List<Student> students = new ArrayList<>();

    private Parent parent;
    MainParentViewModel mViewModel;
    PrefManager prefManager;

    public HomeParentFragment() {
        // Required empty public constructor
    }

    public static HomeParentFragment newInstance(Parent parent) {
        HomeParentFragment fragment = new HomeParentFragment();
        Bundle args = new Bundle();
        args.putSerializable("parent", parent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = PrefManager.getInstance(getContext());
        mViewModel = ViewModelProviders.of(this).get(MainParentViewModel.class);
        mViewModel.getSiswas().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(List<Student> students) {
                progress_content.setVisibility(View.GONE);
                if (students != null) {
                    mAdapterSiswa.updateData(students);
                }
            }
        });

        mAdapterSiswa = new AdapterSiswa(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home_parent, container, false);
        ButterKnife.bind(this, v);
        progress_content.setVisibility(View.VISIBLE);

        recycler_student.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_student.setAdapter(mAdapterSiswa);

        text_nama.setText(prefManager.getOrangtua().getNama());

        nestedScrollView.setOnScrollChangeListener(this);

        image_notifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ParentNotifikasiActivity.class));
            }
        });

        setupAnimation();
        return v;
    }

    void setupAnimation() {
        Animation animation1 = AnimationUtils.loadAnimation(getContext(), R.anim.slide_right);
        text_nama.setAnimation(animation1);
        text_desc.setAnimation(animation1);
        animation1.start();

        Animation animation2 = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
        recycler_student.setAnimation(animation2);
        animation2.start();
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (scrollY > oldScrollY) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(5f);
            }
        }
        if (scrollY < oldScrollY && scrollY == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                appBarLayout.setElevation(0f);
            }
        }
    }

//    @OnClick(R.id.edittext_search_anak)
//    void gotoSearch() {
//        startActivity(new Intent(getContext(), SearchSiswaActivity.class));
//    }
}
