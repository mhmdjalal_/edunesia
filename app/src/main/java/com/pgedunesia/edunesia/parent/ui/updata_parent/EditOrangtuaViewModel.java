package com.pgedunesia.edunesia.parent.ui.updata_parent;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.UpdateParentResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditOrangtuaViewModel extends AndroidViewModel {
    private MutableLiveData<UpdateParentResponse> mUpdateResponse = new MutableLiveData<>();

    ApiInterface apiInterface;
    PrefManager prefManager;

    public EditOrangtuaViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        prefManager = PrefManager.getInstance(getApplication());
    }

    public LiveData<UpdateParentResponse> getUpdateResponse() {
        return mUpdateResponse;
    }

    public void updateOrangtua(String nama, String no_telepon) {
        apiInterface.updateOrangtua(prefManager.getOrangtua().getId(), nama, no_telepon)
                .enqueue(new Callback<UpdateParentResponse>() {
                    @Override
                    public void onResponse(Call<UpdateParentResponse> call, Response<UpdateParentResponse> response) {
                        if (response.isSuccessful()) {
                            mUpdateResponse.postValue(response.body());
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateParentResponse> call, Throwable t) {

                    }
                });
    }
}
