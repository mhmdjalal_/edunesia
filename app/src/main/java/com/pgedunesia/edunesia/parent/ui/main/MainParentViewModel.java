package com.pgedunesia.edunesia.parent.ui.main;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.config.PrefManager;
import com.pgedunesia.edunesia.data.model.Parent;
import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.ParentResponse;
import com.pgedunesia.edunesia.data.model.Student;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainParentViewModel extends AndroidViewModel {
    private MutableLiveData<List<Student>> mSiswas = new MutableLiveData<>();
    private MutableLiveData<Parent> mOrangtua = new MutableLiveData<>();

    ApiInterface apiInterface;
    PrefManager prefManager;

    public MainParentViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
        prefManager = PrefManager.getInstance(application.getBaseContext());
        loadOrangtuaSiswa();
    }

    public LiveData<List<Student>> getSiswas() {
        return mSiswas;
    }

    public LiveData<Parent> getOrangtua() {
        return mOrangtua;
    }

    public void loadOrangtuaSiswa() {
        apiInterface.getOrtuSiswa(prefManager.getOrangtua().getId())
                .enqueue(new Callback<ParentResponse>() {
                    @Override
                    public void onResponse(Call<ParentResponse> call, Response<ParentResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()) {
                                mOrangtua.postValue(response.body().getData());
                                mSiswas.postValue(response.body().getStudents());
                            } else {
                                Log.d("loadOrangtuaSiswa", "failed to get data");
                            }
                        } else {
                            Log.d("loadOrangtuaSiswa", "response failed");
                        }
                    }

                    @Override
                    public void onFailure(Call<ParentResponse> call, Throwable t) {

                    }
                });
    }
}
