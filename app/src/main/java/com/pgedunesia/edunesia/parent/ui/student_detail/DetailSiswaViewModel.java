package com.pgedunesia.edunesia.parent.ui.student_detail;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.model.NilaiUjian;
import com.pgedunesia.edunesia.data.response.ReportNilaiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailSiswaViewModel extends AndroidViewModel {
    private MutableLiveData<List<NilaiUjian>> mNilaiUjian = new MutableLiveData<>();

    ApiInterface apiInterface;

    public DetailSiswaViewModel(@NonNull Application application) {
        super(application);

        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
    }

    public LiveData<List<NilaiUjian>> getNilaiUjian() {
        return mNilaiUjian;
    }

    public void loadNilaiUjian(int siswa_id) {
        apiInterface.getNilaiSiswa(siswa_id)
                .enqueue(new Callback<ReportNilaiResponse>() {
                    @Override
                    public void onResponse(Call<ReportNilaiResponse> call, Response<ReportNilaiResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()) {
                                mNilaiUjian.postValue(response.body().getData());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ReportNilaiResponse> call, Throwable t) {

                    }
                });
    }
}
