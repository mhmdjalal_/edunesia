package com.pgedunesia.edunesia.parent.ui.login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.pgedunesia.edunesia.network.ApiClient;
import com.pgedunesia.edunesia.network.ApiInterface;
import com.pgedunesia.edunesia.data.response.LoginOrangtuaResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginOrtuViewModel extends AndroidViewModel {
    private MutableLiveData<LoginOrangtuaResponse> mLoginResponse = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsLoading = new MutableLiveData<>();

    ApiInterface apiInterface;

    public LoginOrtuViewModel(@NonNull Application application) {
        super(application);
        apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
    }

    public LiveData<LoginOrangtuaResponse> getLoginResponse() {
        return mLoginResponse;
    }

    public LiveData<Boolean> isLoading() {
        return mIsLoading;
    }

    public void loginOrangTua(String username, String password) {
        mIsLoading.postValue(true);
        apiInterface.loginOrangtua(username, password)
                .enqueue(new Callback<LoginOrangtuaResponse>() {
                    @Override
                    public void onResponse(Call<LoginOrangtuaResponse> call, Response<LoginOrangtuaResponse> response) {
                        mIsLoading.postValue(false);
                        if (response.isSuccessful()) {
                            mLoginResponse.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginOrangtuaResponse> call, Throwable t) {
                        mIsLoading.postValue(false);
                    }
                });
    }

}
